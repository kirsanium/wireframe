package com.nsu.bigguys.view.splineEditor.modelPreview;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.model.linal.Vector3D;
import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ModelPreview extends JPanel {
    private ArrayList<Vector3D> splinePoints3D = new ArrayList<>();
    private ArrayList<ArrayList<Vector3D>> actualPoints = new ArrayList<>();
    private ArrayList<ArrayList<Vector3D>> actualCirclePoints = new ArrayList<>();
    private BSpline2D bSpline2D;

    private Matrix3D objectRotationMatrix = Matrix3D.getRotationMatrix(0, 0, 0);

    private double scale = 0.1;
    private double distance = 1000;
    private int circlesPrecision = 50;
    private int rotationPrecision = 10;
    private double splinePrecision = 0.2;

    public ModelPreview(BSpline2D bSpline2D) {
        if (bSpline2D == null) return;
        if (bSpline2D.isEmpty()) return;

        this.bSpline2D = bSpline2D;

        reProcessSpline();
        reProcessRotations();
        reProcessCircles();
    }

    public void setObjectRotationMatrix(Matrix3D objectRotationMatrix) {
        this.objectRotationMatrix = objectRotationMatrix;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawLinesSet(actualPoints, g);
        drawLinesSet(actualCirclePoints, g);
    }

    public void setCirclesPrecision(int precision) {
        circlesPrecision = precision;
        reProcessCircles();
        repaint();
    }

    public void setScale(double scale) {
        this.scale = scale;
        repaint();
    }

    public void setRotationPrecision(int rotationPrecision) {
        this.rotationPrecision = rotationPrecision;
        reProcessRotations();
        repaint();
    }

    public void setSplinePrecision(double splinePrecision) {
        this.splinePrecision = splinePrecision;
        reProcessSpline();
        reProcessRotations();
        reProcessCircles();
        repaint();
    }

    public void setbSpline2D(BSpline2D bSpline2D) {
        this.bSpline2D = bSpline2D;
        reProcessSpline();
        reProcessRotations();
        reProcessCircles();
        repaint();
    }

    public void rotate(Matrix3D rotationMatrix) {
        this.objectRotationMatrix = rotationMatrix.matMul(this.objectRotationMatrix);
    }

    private void drawLinesSet(ArrayList<ArrayList<Vector3D>> linesPoints, Graphics g) {
        for (ArrayList<Vector3D> spline : linesPoints) {
            if (spline.isEmpty()) continue;
            Vector3D first = objectRotationMatrix.matMul(spline.get(0));
            double z_distance = 1000 + first.getZ();
            Point prev = new Point(
                    (int) (scale * (distance * first.getX()) / (z_distance)) + 300,
                    (int) (scale * (distance * first.getY()) / (z_distance)) + 300);

            for (int i = 1; i < spline.size(); i++) {
                Vector3D rotated = objectRotationMatrix.matMul(spline.get(i));
                z_distance = 1000 + rotated.getZ();
                Point cur = new Point(
                        (int) (scale * (distance * rotated.getX()) / (z_distance)) + 300,
                        (int) (scale * (distance * rotated.getY()) / (z_distance)) + 300
                );

                g.drawLine(prev.x, prev.y, cur.x, cur.y);
                prev = cur;
            }
        }
    }

    private ArrayList<Vector3D> createCircle(double t) {
        ArrayList<Vector3D> circlePoints = new ArrayList<>();
        Point value = bSpline2D.getValue(t, splinePrecision);
        if (value == null) return circlePoints;

        Vector3D value3d = new Vector3D(new double[]{value.x, value.y, 0});

        for (int i = 0; i < 50; i++) {
            Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(i * 2.0 * Math.PI / 50, 0, 0);
            circlePoints.add(rotationMatrix.matMul(value3d));
        }

        Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(0.0 * 2.0 * Math.PI / 50, 0, 0);
        circlePoints.add(rotationMatrix.matMul(value3d));

        return circlePoints;
    }

    private void reProcessCircles() {
        actualCirclePoints = new ArrayList<>();

        double t;
        for (t = 0.0; t <= 1.0; t += 1.0 / circlesPrecision) {
            actualCirclePoints.add(createCircle(t));
        }
        if (t > 1.0) {
            actualCirclePoints.add(createCircle(1.0));
        }
    }

    private void reProcessRotations() {
        actualPoints = new ArrayList<>();
        for (int i = 0; i < rotationPrecision; i++) {
            Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(i * 2.0 * Math.PI / rotationPrecision, 0, 0);

            ArrayList<Vector3D> points3dRotated = new ArrayList<>();
            for (Vector3D point3d : splinePoints3D) {
                points3dRotated.add(rotationMatrix.matMul(point3d));
            }

            actualPoints.add(points3dRotated);
        }
    }

    private void reProcessSpline() {
        ArrayList<Point> points2d = Point.fromD(bSpline2D.getCubicPoints(splinePrecision));

        splinePoints3D = new ArrayList<>();
        for (Point point2d : points2d) {
            splinePoints3D.add(new Vector3D(new double[]{point2d.x, point2d.y, 0}));
        }
    }
}
