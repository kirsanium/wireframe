package com.nsu.bigguys.view;

import javax.swing.*;
import java.awt.*;

public class ScenePreference extends JFrame {
    private JTextField aTextField;
    private JTextField bTextField;
    private JTextField znTextField;
    private JTextField zfTextField;
    private JTextField swTextField;
    private JTextField shTextField;
    private JTextField nTextField;
    private JTextField mTextField;
    private JTextField kTextField;
    private JTextField cTextField;
    private JTextField dTextField;
    private JButton okButton;
    private JButton cancelButton;
    private JPanel rootPanel;

    private ScenePreview scenePreview;

    Myint n = new Myint();
    Myint m = new Myint();
    Myint k = new Myint();
    Mydouble a = new Mydouble();
    Mydouble b = new Mydouble();
    Mydouble c = new Mydouble();
    Mydouble d = new Mydouble();
    Mydouble zn = new Mydouble();
    Mydouble zf = new Mydouble();
    Mydouble sw = new Mydouble();
    Mydouble sh = new Mydouble();

    public ScenePreference(ScenePreview scenePreview) {
        this.scenePreview = scenePreview;
        add(rootPanel);
        setParameters(scenePreview.getScene().a, scenePreview.getScene().b,
                scenePreview.getScene().c, scenePreview.getScene().d,
                scenePreview.getScene().n, scenePreview.getScene().m, scenePreview.getScene().k,
                scenePreview.getScene().getZf(), scenePreview.getScene().getZb(),
                scenePreview.getScene().getSw(), scenePreview.getScene().getSh());
        setupListeners();
        setVisible(true);
        setSize(new Dimension(300, 300));
        pack();
    }

    private void setParameters(double a, double b, double c, double d,
                               int n, int m, int k,
                               double zn, double zf, double sw, double sh) {
        this.a.val = a;
        this.b.val = b;
        this.c.val = c;
        this.d.val = d;
        this.n.val = n;
        this.m.val = m;
        this.k.val = k;
        this.zn.val = zn;
        this.zf.val = zf;
        this.sw.val = sw;
        this.sh.val = sh;

        aTextField.setText(Double.toString(a));
        bTextField.setText(Double.toString(b));
        cTextField.setText(Double.toString(c));
        dTextField.setText(Double.toString(d));

        znTextField.setText(Double.toString(zn));
        zfTextField.setText(Double.toString(zf));

        swTextField.setText(Double.toString(sw));
        shTextField.setText(Double.toString(sh));

        nTextField.setText(Integer.toString(n));
        mTextField.setText(Integer.toString(m));
        kTextField.setText(Integer.toString(k));
    }

    private void setupListeners() {
        okButton.addActionListener(e -> {
            doubleTextField(aTextField, a, 0, 1, 0);
            doubleTextField(bTextField, b, 0, 1, 0);
            if (a.val > b.val) {
                JOptionPane.showMessageDialog(null, "a, b parameters constraints failed");
                return;
            }
            doubleTextField(cTextField, c, 0, 360, 0);
            doubleTextField(dTextField, d, 0, 360, 360);
            if (c.val > d.val) {
                JOptionPane.showMessageDialog(null, "c, d parameters constraints failed");
                return;
            }
            intTextField(nTextField, n, 1, 100, 5);
            intTextField(mTextField, m, 1, 100, 5);
            intTextField(kTextField, k, 1, 100, 5);

            doubleTextField(znTextField, zn, 1e-10, 1e100, 3.35);
            doubleTextField(zfTextField, zf, 1e-10, 1e100, 11.35);

            if (zn.val > zf.val) {
                JOptionPane.showMessageDialog(null, "zn, zf parameters constraints failed");
                return;
            }

            doubleTextField(swTextField, sw, 1e-10, 1e100, 1);
            doubleTextField(shTextField, sh, 1e-10, 1e100, 1);


            scenePreview.getScene().setParameters(a.val, b.val, c.val, d.val, n.val, m.val, k.val, zn.val, sw.val, sh.val);
            scenePreview.repaint();
            dispose();
        });

        cancelButton.addActionListener(e -> {
            dispose();
        });
    }

    public void doubleTextField(JTextField textField, Mydouble val, double min, double max, double def) {
        try {
            val.val = Double.parseDouble(textField.getText());
            if (val.val < min || val.val > max) {
                System.out.println("parameter:" + val.val + "bounds: " + min + " " + max);
                JOptionPane.showMessageDialog(null, "parameter out of bounds");
                val.val = def;
            }
        }
        catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "invalid parameter value");
            val.val = def;
        }
    }

    public void intTextField(JTextField textField, Myint val, int min, int max, int def) {
        try {
            val.val = Integer.parseInt(textField.getText());
            if (val.val < min || val.val > max) {
                JOptionPane.showMessageDialog(null, "parameter out of bounds");
                val.val = def;
            }
        }
        catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "invalid parameter value");
            val.val = def;
        }
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new BorderLayout(0, 0));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        rootPanel.add(panel1, BorderLayout.SOUTH);
        final JLabel label1 = new JLabel();
        label1.setText("n");
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label1, gbc);
        final JLabel label2 = new JLabel();
        label2.setText("a");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label2, gbc);
        final JLabel label3 = new JLabel();
        label3.setText("zn");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label3, gbc);
        final JLabel label4 = new JLabel();
        label4.setText("m");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label4, gbc);
        final JLabel label5 = new JLabel();
        label5.setText("b");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label5, gbc);
        final JLabel label6 = new JLabel();
        label6.setText("zf");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label6, gbc);
        final JLabel label7 = new JLabel();
        label7.setText("k");
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label7, gbc);
        final JLabel label8 = new JLabel();
        label8.setText("c");
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label8, gbc);
        final JLabel label9 = new JLabel();
        label9.setText("d");
        gbc = new GridBagConstraints();
        gbc.gridx = 4;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label9, gbc);
        aTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(aTextField, gbc);
        bTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(bTextField, gbc);
        znTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(znTextField, gbc);
        zfTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(zfTextField, gbc);
        final JLabel label10 = new JLabel();
        label10.setText("sh");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label10, gbc);
        final JLabel label11 = new JLabel();
        label11.setText("sw");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        panel1.add(label11, gbc);
        swTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(swTextField, gbc);
        shTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(shTextField, gbc);
        nTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(nTextField, gbc);
        mTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(mTextField, gbc);
        kTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(kTextField, gbc);
        cTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(cTextField, gbc);
        dTextField = new JTextField();
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(dTextField, gbc);
        okButton = new JButton();
        okButton.setText("ok");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(okButton, gbc);
        cancelButton = new JButton();
        cancelButton.setText("cancel");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel1.add(cancelButton, gbc);
        label1.setLabelFor(nTextField);
        label2.setLabelFor(aTextField);
        label3.setLabelFor(znTextField);
        label4.setLabelFor(mTextField);
        label5.setLabelFor(bTextField);
        label6.setLabelFor(zfTextField);
        label7.setLabelFor(kTextField);
        label8.setLabelFor(cTextField);
        label9.setLabelFor(dTextField);
        label10.setLabelFor(shTextField);
        label11.setLabelFor(swTextField);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}

class Mydouble {
    double val;
}

class Myint {
    int val;
}