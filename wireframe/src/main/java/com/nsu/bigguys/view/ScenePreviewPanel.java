package com.nsu.bigguys.view;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.model.linal.Vector3D;
import com.nsu.bigguys.model.objects3D.RevolutionSurface;
import com.nsu.bigguys.model.scene.Cam;
import com.nsu.bigguys.model.scene.Scene;
import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;

public class ScenePreviewPanel extends JPanel {
    private final Vector3D look_at = new Vector3D(0, 0, 0);
    double scale = 1;
    int objects = 0;
    private Scene scene;
    private Point clickCoord = new Point(0, 0);
    private Vector3D firslocation = new Vector3D(-10, 0, 0);
    private Vector3D location = new Vector3D(-10, 0, 0);
    private Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(0, 0, 0);
    private Matrix3D nextrotationMatrix = Matrix3D.getRotationMatrix(0, 0, 0);

    public ScenePreviewPanel() {

        setPreferredSize(new Dimension(500, 500));

        Cam cam = new Cam(location, look_at);
        this.scene = new Scene(cam);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                clickCoord.x = e.getX();
                clickCoord.y = e.getY();
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                double ry = e.getX() - clickCoord.x;
                ry /= 100;
                double my = e.getY() - clickCoord.y;
                my /= 100;

                nextrotationMatrix = Matrix3D.getRotationMatrix(0, ry, 0).matMul(rotationMatrix).matMul(Matrix3D.getRotationMatrix(0, 0, -my));
                location = nextrotationMatrix.matMul(firslocation).mul(scale);

                scene.setCamera(new Cam(location, look_at));

                repaint();
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);

                if (nextrotationMatrix != null) {
                    rotationMatrix = nextrotationMatrix;
                    nextrotationMatrix = null;
                }
            }
        });

        addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                super.mouseWheelMoved(e);
                if (e.getWheelRotation() < 0) {
                    scale = scale * 0.9;
                } else if (e.getWheelRotation() > 0) {
                    scale = scale * 1.1;
                }
                location = rotationMatrix.matMul(firslocation).mul(scale);
                scene.setCamera(new Cam(location, look_at));
                repaint();
            }
        });

    }

    public void addObject(BSpline2D bSpline2D) {
        String name = "Object" + objects;
        objects++;
        scene.addObject(new RevolutionSurface(bSpline2D, scene.m, scene.n, scene.k, scene.a, scene.b, scene.c, scene.d), name);
        repaint();
    }

    public void placeObject(BSpline2D bSpline2D) {
        String name = "Object" + objects;
        objects++;
        bSpline2D.scale = scene.maxAABB.maxValues.getX() / 1000 + 1;
        scene.placeObject(new RevolutionSurface(bSpline2D, scene.m, scene.n, scene.k, scene.a, scene.b, scene.c, scene.d), name);
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        scene.render(g);
    }

    public void objectPreferences() {
        new ObjectPreferences(scene, this);
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
        repaint();
    }
}
