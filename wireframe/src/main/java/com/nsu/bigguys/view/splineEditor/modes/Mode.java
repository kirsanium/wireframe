package com.nsu.bigguys.view.splineEditor.modes;

import com.nsu.bigguys.model.spline.Point;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public interface Mode extends Serializable {
    /**
     * action should be performed on mouse click
     *
     * @param x x click coordinate
     * @param y y click coordinate
     */
    void onClicked(int x, int y);

    /**
     * action should be performed on mouse drag
     *
     * @param x x mouse coordinate
     * @param y y mouse coordinate
     */
    void onDragged(int x, int y);

    /**
     * action should be performed on mouse move
     *
     * @param x x mouse coordinate
     * @param y y mouse coordinate
     */
    void onMoved(int x, int y);

    /**
     * action should be performed on mouse release
     *
     * @param x x release coordinate
     * @param y y release coordinate
     */
    void onReleased(int x, int y);

    /**
     * action should be performed on mouse pressed
     *
     * @param x x mouse coordinate
     * @param y y mouse coordinate
     */
    void onPressed(int x, int y);

    /**
     * perform some mode-specific paint
     */
    void onRepaint(Graphics graphics);

    /**
     * check if it possible to switch mode in the moment
     *
     * @return true, if mode busy, false otherwise
     */
    boolean isInAction();

    /**
     * get mode-specific or action-specific control actualPoints,
     * may not match to actual control actualPoints set
     *
     * @return control actualPoints ArrayList
     */
    ArrayList<Point> getFramePoints();
}
