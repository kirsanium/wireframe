package com.nsu.bigguys.view.splineEditor.modes;

import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;
import com.nsu.bigguys.view.splineEditor.SplineEditor;

import java.awt.*;
import java.util.ArrayList;

public class SelectionMode implements Mode {
    private SplineEditor splineEditor;
    private Integer nearestNeighborIndex;

    public SelectionMode(SplineEditor splineEditor) {
        this.splineEditor = splineEditor;
        this.splineEditor.repaint();
    }

    @Override
    public void onClicked(int x, int y) {
        if (nearestNeighborIndex != null) {
            splineEditor.getControlpoints().get(nearestNeighborIndex).x = x;
            splineEditor.getControlpoints().get(nearestNeighborIndex).y = y;
            splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(splineEditor.getControlpoints())));
        }
        splineEditor.repaint();
    }

    @Override
    public void onDragged(int x, int y) {
        if (nearestNeighborIndex != null) {
            splineEditor.getControlpoints().get(nearestNeighborIndex).x = x;
            splineEditor.getControlpoints().get(nearestNeighborIndex).y = y;
            splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(splineEditor.getControlpoints())));
            splineEditor.repaint();
        }
    }

    @Override
    public void onMoved(int x, int y) {
        nearestNeighborIndex = splineEditor.findNearestPoint(new Point(x, y));
        splineEditor.repaint();
    }

    @Override
    public void onReleased(int x, int y) {
        // no action
    }

    @Override
    public void onPressed(int x, int y) {
        // no action
    }

    @Override
    public void onRepaint(Graphics g) {
        DeletionMode.paintNearestPoint(g, nearestNeighborIndex, splineEditor);
    }

    @Override
    public boolean isInAction() {
        return false;
    }

    @Override
    public ArrayList<Point> getFramePoints() {
        return splineEditor.getControlpoints();
    }
}
