package com.nsu.bigguys.view.splineEditor.modelPreview;


import com.nsu.bigguys.model.linal.Matrix3D;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DynamicRotatingModel {
    private Timer timer = null;

    public DynamicRotatingModel(ModelPreview modelPreview, Matrix3D rotationMatrix) {
        SwingUtilities.invokeLater(() -> {
            ActionListener actionListener = e -> {
                modelPreview.rotate(rotationMatrix);
                modelPreview.repaint();
            };
            timer = new Timer(40, actionListener);
            timer.start();
        });
    }
}
