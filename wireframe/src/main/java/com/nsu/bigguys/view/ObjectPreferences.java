package com.nsu.bigguys.view;

import com.nsu.bigguys.model.linal.Vector3D;
import com.nsu.bigguys.model.objects3D.RevolutionSurface;
import com.nsu.bigguys.model.scene.Scene;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class ObjectPreferences extends JFrame implements WindowListener {
    JColorChooser jColorChooser;
    private JComboBox comboBox1;
    private JPanel panel1;
    private JPanel colorPicker;
    private JPanel MoveRotate;
    private JPanel move;
    private JPanel rotate;
    private JSlider xRotateSlider;
    private JSlider yRotateSlider;
    private JSlider zRotateSlider;
    private JPanel xMove;
    private JTextField xMoveTextField;
    private JTextField yMoveTextField;
    private JTextField zMoveTextField;
    private JPanel namePanel;
    private JTextField nameTextField;
    private JPanel xRotatePanel;
    private JPanel rotateNext;
    private JPanel yRotate;
    private JPanel rotateNext2;
    private JPanel zRotatePanel;
    private JLabel xRotateLabel;
    private JLabel yRotateLabel;
    private JLabel zRotateLabel;
    private JPanel buttonsPanel;
    private JPanel cancelButtonPanel;
    private JButton cancelButton;
    private JPanel okButtonPanel;
    private JButton okButton;
    private JPanel editSplineButtonPanel;
    private JButton editSplineButton;
    private Scene scene;
    private ScenePreviewPanel scenePreview;
    private String objectName;

    public ObjectPreferences(Scene scene, ScenePreviewPanel scenePreview) {
        super("Object preferences");

        this.scenePreview = scenePreview;
        this.scene = scene;

        jColorChooser = new JColorChooser();
        jColorChooser.setPreviewPanel(new JPanel());
        colorPicker.add(jColorChooser.getChooserPanels()[3]);
        setupListeners();
        add(panel1);
        setVisible(true);
        setSize(new Dimension(520, 500));
        setResizable(false);
    }

    private void selectObject(String name) {
        objectName = name;
        scene.setSelectedObj(objectName);
        scenePreview.repaint();

        RevolutionSurface object = scene.getObjectByName(name);
        var angles = object.getAngles();
        xRotateSlider.setValue(angles.get(0));
        yRotateSlider.setValue(angles.get(1));
        zRotateSlider.setValue(angles.get(2));

        Vector3D coords = object.getCoords();

        xMoveTextField.setText(Double.toString(coords.getX()));
        yMoveTextField.setText(Double.toString(coords.getY()));
        zMoveTextField.setText(Double.toString(coords.getZ()));
    }

    private void setupListeners() {
        xRotateSlider.setMinimum(0);
        yRotateSlider.setMinimum(0);
        zRotateSlider.setMinimum(0);

        xRotateSlider.setMaximum(360);
        yRotateSlider.setMaximum(360);
        zRotateSlider.setMaximum(360);

        xRotateSlider.setValue(0);
        yRotateSlider.setValue(0);
        zRotateSlider.setValue(0);

        updateAnglesListener(xRotateSlider, xRotateLabel);
        updateAnglesListener(yRotateSlider, yRotateLabel);
        updateAnglesListener(zRotateSlider, zRotateLabel);

        updateCoordsListener(xMoveTextField);
        updateCoordsListener(yMoveTextField);
        updateCoordsListener(zMoveTextField);

        addWindowListener(this);

        for (String name : scene.getObejctsNames()) {
            comboBox1.addItem(name);
        }

        comboBox1.addActionListener(e -> {
            JComboBox cb = (JComboBox) e.getSource();
            selectObject((String) cb.getSelectedItem());
        });

        editSplineButton.addActionListener(e -> {
        });

        okButton.addActionListener(e -> {
            scene.dropSelectedObj();
            scenePreview.repaint();
            dispose();
        });

        cancelButton.addActionListener(e -> {
            scene.dropSelectedObj();
            scenePreview.repaint();
            dispose();
        });

        jColorChooser.getSelectionModel().addChangeListener(e -> {
            if (objectName == null) return;
            scene.getObjectByName(objectName).setColor(jColorChooser.getColor());
            scenePreview.repaint();
        });

    }

    private void updateAnglesListener(JSlider slider, JLabel label) {
        slider.addChangeListener(e -> {
            label.setText(Integer.toString(slider.getValue()));

            if (objectName == null) return;
            scene.getObjectByName(objectName).setRotationAngle(
                    xRotateSlider.getValue(),
                    yRotateSlider.getValue(),
                    zRotateSlider.getValue()
            );
            scene.recompute();
            scenePreview.repaint();
        });
    }

    private void updateCoordsListener(JTextField textField) {
        textField.addActionListener(e -> {
            if (objectName == null) return;
            double x = Double.parseDouble(xMoveTextField.getText());
            double y = Double.parseDouble(yMoveTextField.getText());
            double z = Double.parseDouble(zMoveTextField.getText());

            scene.getObjectByName(objectName).setCoords(x, y, z);
            scene.recompute();
            scenePreview.repaint();
        });
    }

    @Override
    public void windowOpened(WindowEvent e) {
        // no action
    }

    @Override
    public void windowClosing(WindowEvent e) {
        scene.dropSelectedObj();
        scenePreview.repaint();
        dispose();
    }

    @Override
    public void windowClosed(WindowEvent e) {
        // no action
    }

    @Override
    public void windowIconified(WindowEvent e) {
        // no action
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // no action
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // no action
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // no action
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new BorderLayout(0, 0));
        comboBox1 = new JComboBox();
        panel1.add(comboBox1, BorderLayout.NORTH);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout(0, 0));
        panel1.add(panel2, BorderLayout.CENTER);
        colorPicker = new JPanel();
        colorPicker.setLayout(new BorderLayout(0, 0));
        panel2.add(colorPicker, BorderLayout.NORTH);
        MoveRotate = new JPanel();
        MoveRotate.setLayout(new BorderLayout(0, 0));
        panel2.add(MoveRotate, BorderLayout.CENTER);
        move = new JPanel();
        move.setLayout(new BorderLayout(0, 0));
        MoveRotate.add(move, BorderLayout.CENTER);
        xMove = new JPanel();
        xMove.setLayout(new BorderLayout(0, 0));
        move.add(xMove, BorderLayout.NORTH);
        xMoveTextField = new JTextField();
        xMove.add(xMoveTextField, BorderLayout.CENTER);
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new BorderLayout(0, 0));
        move.add(panel3, BorderLayout.CENTER);
        yMoveTextField = new JTextField();
        panel3.add(yMoveTextField, BorderLayout.NORTH);
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new BorderLayout(0, 0));
        panel3.add(panel4, BorderLayout.CENTER);
        zMoveTextField = new JTextField();
        panel4.add(zMoveTextField, BorderLayout.NORTH);
        rotate = new JPanel();
        rotate.setLayout(new BorderLayout(0, 0));
        rotate.setEnabled(true);
        MoveRotate.add(rotate, BorderLayout.EAST);
        xRotatePanel = new JPanel();
        xRotatePanel.setLayout(new BorderLayout(0, 0));
        rotate.add(xRotatePanel, BorderLayout.NORTH);
        xRotateSlider = new JSlider();
        xRotatePanel.add(xRotateSlider, BorderLayout.CENTER);
        xRotateLabel = new JLabel();
        xRotateLabel.setText("0");
        xRotatePanel.add(xRotateLabel, BorderLayout.EAST);
        rotateNext = new JPanel();
        rotateNext.setLayout(new BorderLayout(0, 0));
        rotate.add(rotateNext, BorderLayout.CENTER);
        yRotate = new JPanel();
        yRotate.setLayout(new BorderLayout(0, 0));
        rotateNext.add(yRotate, BorderLayout.NORTH);
        yRotateSlider = new JSlider();
        yRotate.add(yRotateSlider, BorderLayout.CENTER);
        yRotateLabel = new JLabel();
        yRotateLabel.setText("0");
        yRotate.add(yRotateLabel, BorderLayout.EAST);
        rotateNext2 = new JPanel();
        rotateNext2.setLayout(new BorderLayout(0, 0));
        rotateNext.add(rotateNext2, BorderLayout.CENTER);
        zRotatePanel = new JPanel();
        zRotatePanel.setLayout(new BorderLayout(0, 0));
        rotateNext2.add(zRotatePanel, BorderLayout.NORTH);
        zRotateSlider = new JSlider();
        zRotatePanel.add(zRotateSlider, BorderLayout.CENTER);
        zRotateLabel = new JLabel();
        zRotateLabel.setText("0");
        zRotatePanel.add(zRotateLabel, BorderLayout.EAST);
        namePanel = new JPanel();
        namePanel.setLayout(new BorderLayout(0, 0));
        panel1.add(namePanel, BorderLayout.SOUTH);
        nameTextField = new JTextField();
        namePanel.add(nameTextField, BorderLayout.CENTER);
        buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BorderLayout(0, 0));
        namePanel.add(buttonsPanel, BorderLayout.SOUTH);
        cancelButtonPanel = new JPanel();
        cancelButtonPanel.setLayout(new BorderLayout(0, 0));
        buttonsPanel.add(cancelButtonPanel, BorderLayout.WEST);
        cancelButton = new JButton();
        cancelButton.setText("cancel");
        cancelButtonPanel.add(cancelButton, BorderLayout.CENTER);
        okButtonPanel = new JPanel();
        okButtonPanel.setLayout(new BorderLayout(0, 0));
        buttonsPanel.add(okButtonPanel, BorderLayout.EAST);
        okButton = new JButton();
        okButton.setText("ok");
        okButtonPanel.add(okButton, BorderLayout.CENTER);
        editSplineButtonPanel = new JPanel();
        editSplineButtonPanel.setLayout(new BorderLayout(0, 0));
        buttonsPanel.add(editSplineButtonPanel, BorderLayout.CENTER);
        editSplineButton = new JButton();
        editSplineButton.setText("edit spline");
        editSplineButtonPanel.add(editSplineButton, BorderLayout.CENTER);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }

}
