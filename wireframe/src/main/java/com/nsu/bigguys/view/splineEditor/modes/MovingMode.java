package com.nsu.bigguys.view.splineEditor.modes;

import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;
import com.nsu.bigguys.view.splineEditor.SplineEditor;

import java.awt.*;
import java.util.ArrayList;

public class MovingMode implements Mode {
    private SplineEditor splineEditor;
    private Point pressedCoords;

    private Point AABBlefttop = null;
    private int AABBwidth = 0;
    private int AABBheight = 0;
    private ArrayList<Point> movedControlpoints;

    public MovingMode(SplineEditor splineEditor) {
        this.splineEditor = splineEditor;
        calculateAABB();

        movedControlpoints = new ArrayList<>();

        for (int i = 0; i < splineEditor.getControlpoints().size(); i++) {
            movedControlpoints.add(new Point(splineEditor.getControlpoints().get(i)));
        }
    }

    /**
     * calculate axis aligned bounding box
     */
    private void calculateAABB() {
        if (splineEditor.getControlpoints().isEmpty()) return;

        AABBlefttop = new Point(splineEditor.getControlpoints().get(0));

        for (Point point : splineEditor.getControlpoints()) {
            if (point.y < AABBlefttop.y) {
                AABBlefttop.y = point.y;
            }
            if (point.x < AABBlefttop.x) {
                AABBlefttop.x = point.x;
            }
        }
        for (Point point : splineEditor.getControlpoints()) {
            if (point.x - AABBlefttop.x > AABBwidth) {
                AABBwidth = point.x - AABBlefttop.x;
            }
            if (point.y - AABBlefttop.y > AABBheight) {
                AABBheight = point.y - AABBlefttop.y;
            }
        }
    }

    private Point boundedCoordinates(int x, int y) {
        if (x + AABBwidth > splineEditor.getCanvasSize().width) {
            x = splineEditor.getCanvasSize().width - AABBwidth;
        }
        if (y + AABBheight > splineEditor.getCanvasSize().height) {
            y = splineEditor.getCanvasSize().height - AABBheight;
        }
        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }

        return new Point(x, y);
    }

    @Override
    public void onClicked(int x, int y) {
        // no action
    }

    @Override
    public void onDragged(int x, int y) {
        if (AABBlefttop == null) return;

        Point movedAABBlefttop = boundedCoordinates(
                AABBlefttop.x + x - pressedCoords.x,
                AABBlefttop.y + y - pressedCoords.y);

        for (int i = 0; i < splineEditor.getControlpoints().size(); i++) {
            movedControlpoints.set(i, new Point(splineEditor.getControlpoints().get(i)));
            movedControlpoints.get(i).x += movedAABBlefttop.x - AABBlefttop.x;
            movedControlpoints.get(i).y += movedAABBlefttop.y - AABBlefttop.y;
        }
        splineEditor.setbSpline((new BSpline2D(Point.fromScreenPoints(movedControlpoints))));
        splineEditor.repaint();
    }

    @Override
    public void onMoved(int x, int y) {
        // no action
    }

    @Override
    public void onReleased(int x, int y) {
        AABBlefttop = boundedCoordinates(
                AABBlefttop.x + x - pressedCoords.x,
                AABBlefttop.y + y - pressedCoords.y);
        for (int i = 0; i < splineEditor.getControlpoints().size(); i++) {
            splineEditor.getControlpoints().set(i, new Point(movedControlpoints.get(i)));
        }
        splineEditor.repaint();
    }

    @Override
    public void onPressed(int x, int y) {
        pressedCoords = new Point(x, y);
    }

    @Override
    public void onRepaint(Graphics graphics) {
        // no action
    }

    @Override
    public boolean isInAction() {
        return false;
    }

    @Override
    public ArrayList<Point> getFramePoints() {
        if (movedControlpoints != null) return movedControlpoints;
        return splineEditor.getControlpoints();
    }

}
