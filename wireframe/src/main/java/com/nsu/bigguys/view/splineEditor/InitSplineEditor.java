package com.nsu.bigguys.view.splineEditor;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.view.MainFrame;
import com.nsu.bigguys.view.ScenePreview;
import com.nsu.bigguys.view.ScenePreviewPanel;
import com.nsu.bigguys.view.splineEditor.modelPreview.DynamicRotatingModel;
import com.nsu.bigguys.view.splineEditor.modelPreview.ModelPreview;
import com.nsu.bigguys.view.splineEditor.modelPreview.mpreivew;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class InitSplineEditor extends MainFrame {
    SplineEditor splineEditor;
    ScenePreviewPanel scene;

    public InitSplineEditor(ScenePreviewPanel scene) {
        super(1200, 655, "Spline Editor");
        this.scene = scene;
        setVisible(true);
        setResizable(false);
        try {
            addSubMenu("Tools", KeyEvent.VK_T);
            addMenuItem("Tools/Add point", "add point mode", KeyEvent.VK_SPACE, "add.png", "onAdd");
            addMenuItem("Tools/Delete point", "delete nearest point", KeyEvent.VK_SPACE, "delete.png", "onDelete");
            addMenuItem("Tools/Move", "move all control actualPoints", KeyEvent.VK_SPACE, "move.png", "onMove");
            addMenuItem("Tools/SelectingMode", "Enter / exit selecting mode", KeyEvent.VK_SPACE, "select.png", "onSelectingMode");
            addMenuItem("Tools/Add on scene", "Add created object to scene", KeyEvent.VK_SPACE, "preview.png", "onFinish");

            addMenuItem("Tools/Frame", "show / hide frame broken line", KeyEvent.VK_SPACE, "frameline.png", "onFrameVisible");
            addMenuItem("Tools/Cleanup", "delete all control actualPoints", KeyEvent.VK_SPACE, "clean.png", "onCleanup");
            addMenuItem("Tools/Preview", "Preview 3d model", KeyEvent.VK_SPACE, "preview.png", "onPreview");

            addToolBarButton("Tools/Add point");
            addToolBarButton("Tools/Delete point");
            addToolBarButton("Tools/Move");
            addToolBarButton("Tools/SelectingMode");
            addToolBarSeparator();
            addToolBarButton("Tools/Frame");
            addToolBarButton("Tools/Cleanup");
            addToolBarButton("Tools/Preview");
            addToolBarSeparator();
            addToolBarButton("Tools/Add on scene");

            splineEditor = new SplineEditor();
            ModelPreview modelPreview = new ModelPreview(null);

            JPanel modelPreviewPanel = new JPanel();
            modelPreviewPanel.setVisible(true);
            modelPreview.setPreferredSize(new Dimension(500, 735));
            modelPreview.setScale(0.5);

            splineEditor.setModelPreview(modelPreview);

            double rx = 30.0 / 360 * 2 * Math.PI;
            double ry = 0;
            double rz = 270.0 / 360 * 2 * Math.PI;

            Matrix3D initRotation = Matrix3D.getRotationMatrix(rx, ry, rz);
            modelPreview.setObjectRotationMatrix(initRotation);
            rx = 0;
            ry = 2.0 / 360 * 2 * Math.PI;
            rz = 0;
            Matrix3D dynamicRotation = Matrix3D.getRotationMatrix(rx, ry, rz);
            new DynamicRotatingModel(modelPreview, dynamicRotation);

            modelPreviewPanel.setLayout(new BorderLayout(0, 0));
            modelPreviewPanel.add(modelPreview, BorderLayout.WEST);
            JPanel rootPanel = new JPanel();
            rootPanel.setLayout(new BorderLayout(0, 0));
            rootPanel.add(modelPreviewPanel, BorderLayout.CENTER);
            rootPanel.add(splineEditor, BorderLayout.WEST);
            add(rootPanel);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void onExit() {
        dispose();
    }

    public void onAdd() {
        splineEditor.switchAdditionMode();
    }

    public void onDelete() {
        splineEditor.switchDeleteMode();
    }

    public void onMove() {
        splineEditor.switchMovingMode();
    }

    public void onSelectingMode() {
        splineEditor.switchSelectMode();
    }

    public void onScene() {
        new ScenePreview();
    }

    public void onFrameVisible() {
        splineEditor.switchFrameVisibility();
    }

    public void onCleanup() {
        splineEditor.cleanup();
    }

    public void onPreview() {
        new mpreivew(splineEditor.bSpline);
    }

    public void onFinish() {
        scene.placeObject(splineEditor.bSpline);
        dispose();
    }
}
