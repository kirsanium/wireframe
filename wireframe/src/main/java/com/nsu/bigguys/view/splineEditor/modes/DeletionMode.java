package com.nsu.bigguys.view.splineEditor.modes;

import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;
import com.nsu.bigguys.view.splineEditor.SplineEditor;

import java.awt.*;
import java.util.ArrayList;

public class DeletionMode implements Mode {
    private SplineEditor splineEditor;
    private boolean inAction;
    private Integer nearestNeighborIndex;
    private Integer deletingPointIndex;

    public DeletionMode(SplineEditor splineEditor) {
        this.splineEditor = splineEditor;
        this.splineEditor.repaint();
    }

    static void paintNearestPoint(Graphics g, Integer nearestNeighborIndex, SplineEditor splineEditor) {
        Color color = g.getColor();
        g.setColor(Color.BLUE);
        if (nearestNeighborIndex != null) {
            g.drawOval(
                    splineEditor.getControlpoints().get(nearestNeighborIndex).x - 2,
                    splineEditor.getControlpoints().get(nearestNeighborIndex).y - 2,
                    4,
                    4);
            g.fillOval(
                    splineEditor.getControlpoints().get(nearestNeighborIndex).x - 2,
                    splineEditor.getControlpoints().get(nearestNeighborIndex).y - 2,
                    4,
                    4);
        }
        g.setColor(color);
    }

    @Override
    public void onClicked(int x, int y) {
        if (nearestNeighborIndex != null) {
            splineEditor.getControlpoints().remove(nearestNeighborIndex.intValue());
            nearestNeighborIndex = splineEditor.findNearestPoint(new Point(x, y));
            splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(splineEditor.getControlpoints())));
            splineEditor.repaint();
        }
    }

    @Override
    public void onDragged(int x, int y) {
        deletingPointIndex = nearestNeighborIndex;
        if (deletingPointIndex == null) return;
        ArrayList<Point> deletion = new ArrayList<>(splineEditor.getControlpoints());
        deletion.remove(deletingPointIndex.intValue());

        splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(deletion)));
        splineEditor.repaint();
    }

    @Override
    public void onMoved(int x, int y) {
        nearestNeighborIndex = splineEditor.findNearestPoint(new Point(x, y));
        splineEditor.repaint();
    }

    @Override
    public void onReleased(int x, int y) {
        if (deletingPointIndex != null) {
            splineEditor.getControlpoints().remove(deletingPointIndex.intValue());
            deletingPointIndex = null;
            nearestNeighborIndex = splineEditor.findNearestPoint(new Point(x, y));
            splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(splineEditor.getControlpoints())));
            splineEditor.repaint();
        }
    }

    @Override
    public void onPressed(int x, int y) {
        // no action
    }

    @Override
    public void onRepaint(Graphics g) {
        paintNearestPoint(g, nearestNeighborIndex, splineEditor);
    }

    @Override
    public boolean isInAction() {
        return inAction;
    }

    @Override
    public ArrayList<Point> getFramePoints() {
        if (deletingPointIndex == null) return splineEditor.getControlpoints();

        ArrayList<Point> framepoints = new ArrayList<>(splineEditor.getControlpoints());
        framepoints.remove(deletingPointIndex.intValue());

        return framepoints;
    }

}
