package com.nsu.bigguys.view.splineEditor;

import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;
import com.nsu.bigguys.model.spline.PointD;
import com.nsu.bigguys.view.splineEditor.modelPreview.ModelPreview;
import com.nsu.bigguys.view.splineEditor.modes.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class SplineEditor extends JPanel {
    public static final Dimension canvasSize = new Dimension(700, 600);
    private ArrayList<Point> controlpoints = new ArrayList<>();
    BSpline2D bSpline;
    double scale = 10.;
    private Mode mode = new AdditionMode(this);
    private BSpline2D last_notified = bSpline;
    private ModelPreview modelPreview = null;
    private boolean frameVisible = true;

    public SplineEditor() {
        setup();
        bSpline = new BSpline2D(new ArrayList<>());
    }

    public SplineEditor(ArrayList<PointD> controlpoints) {
        setup();
        if (controlpoints.isEmpty()) {
            bSpline = new BSpline2D(new ArrayList<>());
        }

        double minX = controlpoints.get(0).x;
        double maxX = controlpoints.get(0).x;
        double minY = controlpoints.get(0).y;
        double maxY = controlpoints.get(0).y;

        for (PointD point : controlpoints) {
            if (point.x < minX) minX = point.x;
            if (point.x > maxX) maxX = point.x;
            if (point.y < minY) minY = point.y;
            if (point.y > maxY) maxY = point.y;
        }

        double maxdifference = maxX - minX;
        if ((maxY - minY) > maxdifference) maxdifference = maxY - minY;

        double maxCanvasDifference = (canvasSize.height > canvasSize.width) ? canvasSize.height : canvasSize.width;

        scale = maxCanvasDifference / maxdifference;

        for (PointD pointD : controlpoints) {
            this.controlpoints.add(new Point((int) (scale * pointD.x), (int) (scale * pointD.y)));
        }
    }

    public static double euclidianMetric(Point p1, Point p2) {
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    public void setModelPreview(ModelPreview modelPreview) {
        this.modelPreview = modelPreview;
    }

    private void notifyModelPreview() {
        if (modelPreview != null && last_notified != bSpline) {
            modelPreview.setbSpline2D(bSpline);
            last_notified = bSpline;
        }
    }

    private void setup() {
        setPreferredSize(canvasSize);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Point boundedCoords = getBoundedCoords(e.getX(), e.getY());
                mode.onClicked(boundedCoords.x, boundedCoords.y);
                notifyModelPreview();
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                Point boundedCoords = getBoundedCoords(e.getX(), e.getY());
                mode.onReleased(boundedCoords.x, boundedCoords.y);
                notifyModelPreview();
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                Point boundedCoords = getBoundedCoords(e.getX(), e.getY());
                mode.onPressed(boundedCoords.x, boundedCoords.y);
                notifyModelPreview();
            }
        });

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                Point boundedCoords = getBoundedCoords(e.getX(), e.getY());
                mode.onDragged(boundedCoords.x, boundedCoords.y);
                notifyModelPreview();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                Point boundedCoords = getBoundedCoords(e.getX(), e.getY());
                mode.onMoved(boundedCoords.x, boundedCoords.y);
                notifyModelPreview();
            }
        });
    }

    public Integer findNearestPoint(Point point) {
        if (controlpoints.isEmpty()) return null;
        double mindist = euclidianMetric(controlpoints.get(0), point);
        int minindex = 0;
        for (int i = 0; i < controlpoints.size(); i++) {
            double dist = euclidianMetric(controlpoints.get(i), point);
            if (dist < mindist) {
                mindist = dist;
                minindex = i;
            }
        }
        return minindex;
    }

    private Point getBoundedCoords(int x, int y) {
        if (x < 0) x = 0;
        if (x > canvasSize.width - 1) x = canvasSize.width - 1;
        if (y < 0) y = 0;
        if (y > canvasSize.height - 1) y = canvasSize.height - 1;

        return new Point(x, y);
    }

    private void paintScale(Graphics graphics) {
        double unit = 1 * scale;

        for (double i = 0; i < canvasSize.width; i += unit) {
            graphics.drawLine((int) i, canvasSize.height / 2 - 2, (int) i, canvasSize.height / 2 + 2);
        }

        for (double i = 0; i < canvasSize.height; i += unit) {
            graphics.drawLine(canvasSize.width / 2 - 2, (int) i, canvasSize.width / 2 + 2, (int) i);
        }
    }

    private void paintSplineFrame(Graphics graphics) {
        ArrayList<Point> framepoints = mode.getFramePoints();

        if (framepoints.isEmpty()) return;
        Color color = graphics.getColor();
        graphics.setColor(new Color(117, 193, 201));

        for (Point point : framepoints) {
            graphics.drawOval(point.x - 1, point.y - 1, 2, 2);
        }

        Point last = framepoints.get(framepoints.size() - 1);
        graphics.fillRect(last.x - 2, last.y - 2, 4, 4);

        for (int i = 0; i < framepoints.size() - 1; i++) {
            graphics.drawLine(
                    framepoints.get(i).x,
                    framepoints.get(i).y,
                    framepoints.get(i + 1).x,
                    framepoints.get(i + 1).y);
        }
        graphics.setColor(color);
    }

    public void switchAdditionMode() {
        if (mode.isInAction()) return;
        if (mode.getClass() == AdditionMode.class) return;
        mode = new AdditionMode(this);
    }

    public void switchDeleteMode() {
        if (mode.isInAction()) return;
        if (mode.getClass() == DeletionMode.class) {
            mode = new AdditionMode(this);
        } else {
            mode = new DeletionMode(this);
        }
    }

    public void switchMovingMode() {
        if (mode.isInAction()) return;
        if (mode.getClass() == MovingMode.class) {
            mode = new AdditionMode(this);
        } else {
            mode = new MovingMode(this);
        }
    }

    public void switchSelectMode() {
        if (mode.isInAction()) return;
        if (mode.getClass() == SelectionMode.class) {
            mode = new AdditionMode(this);
        } else {
            mode = new SelectionMode(this);
        }
    }

    public void switchFrameVisibility() {
        frameVisible = !frameVisible;
        repaint();
    }

    public void cleanup() {
        if (mode.isInAction()) return;
        controlpoints.clear();
        bSpline = new BSpline2D(Point.fromScreenPoints(controlpoints));
        mode = new AdditionMode(this);
        repaint();
        notifyModelPreview();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.repaint();
        Color color = g.getColor();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, canvasSize.width, canvasSize.height);
        g.setColor(new Color(83, 83, 83));
        g.drawRect(-1, -1, canvasSize.width + 2, canvasSize.height + 2);
        g.drawLine(canvasSize.width / 2, 0, canvasSize.width / 2, canvasSize.height);
        g.drawLine(0, canvasSize.height / 2, canvasSize.width, canvasSize.height / 2);
        paintScale(g);
        g.setColor(color);
        if (frameVisible) paintSplineFrame(g);
        bSpline.drawCubic(g, 0.02);
        mode.onRepaint(g);
    }

    public Dimension getCanvasSize() {
        return canvasSize;
    }

    public ArrayList<Point> getControlpoints() {
        return controlpoints;
    }

    public void setbSpline(BSpline2D bSpline) {
        this.bSpline = bSpline;
    }
}
