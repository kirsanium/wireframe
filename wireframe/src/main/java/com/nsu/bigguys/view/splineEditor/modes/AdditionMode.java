package com.nsu.bigguys.view.splineEditor.modes;

import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.Point;
import com.nsu.bigguys.view.splineEditor.SplineEditor;

import java.awt.*;
import java.util.ArrayList;

public class AdditionMode implements Mode {
    private SplineEditor splineEditor;
    private boolean inAction = false;
    private Point addingPoint = null;

    public AdditionMode(SplineEditor splineEditor) {
        this.splineEditor = splineEditor;
        this.splineEditor.repaint();
    }

    @Override
    public void onClicked(int x, int y) {
        splineEditor.getControlpoints().add(new Point(x, y));
        splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(splineEditor.getControlpoints())));

        splineEditor.repaint();
    }

    @Override
    public void onDragged(int x, int y) {
        inAction = true;
        if (addingPoint == null) {
            addingPoint = new Point(x, y);
        } else {
            addingPoint.x = x;
            addingPoint.y = y;
        }

        ArrayList<Point> addition = new ArrayList<>(splineEditor.getControlpoints());
        addition.add(addingPoint);

        splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(addition)));
        splineEditor.repaint();
    }

    @Override
    public void onMoved(int x, int y) {
        // no action
    }

    @Override
    public void onReleased(int x, int y) {
        if (addingPoint == null) return;
        splineEditor.getControlpoints().add(addingPoint);
        addingPoint = null;
        splineEditor.setbSpline(new BSpline2D(Point.fromScreenPoints(splineEditor.getControlpoints())));
        splineEditor.repaint();
        inAction = false;
    }

    @Override
    public void onPressed(int x, int y) {
        // no action
    }

    @Override
    public void onRepaint(Graphics g) {
        // no action
    }

    @Override
    public boolean isInAction() {
        return inAction;
    }

    @Override
    public ArrayList<Point> getFramePoints() {
        if (addingPoint == null) return splineEditor.getControlpoints();

        ArrayList<Point> framepoints = new ArrayList<>(splineEditor.getControlpoints());
        framepoints.add(addingPoint);

        return framepoints;
    }
}
