package com.nsu.bigguys.model.objects3D;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.model.linal.Vector3D;
import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.PointD;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

@Getter
@Setter
public class RevolutionSurface implements Serializable {
    public AABB aabb;
    private BSpline2D generatrixCurve;
    private Matrix3D objectRotationMatrix;
    private ArrayList<ArrayList<Vector3D>> actualPoints;
    private ArrayList<ArrayList<Vector3D>> circles;
    private ArrayList<ArrayList<Vector3D>> objectPoints;
    private ArrayList<Vector3D> splinePoints3D = new ArrayList<>();
    private Color color;
    private int xRotationAngle = 0;
    private int yRotationAngle = 0;
    private int zRotationAngle = 0;
    private double xMove = 0;
    private double yMove = 0;
    private double zMove = 0;
    private int rotationPrecision;
    private int n;
    private int k;
    private double a;
    private double b;
    private double c;
    private double d;

    public RevolutionSurface(BSpline2D generatrixCurve, int rotationPrecision, int n, int k, double a, double b, double c, double d) {
        Random random = new Random();

        int R = random.nextInt(255);
        int G = random.nextInt(255);
        int B = random.nextInt(255);

        color = new Color(R, G, B);
        this.generatrixCurve = generatrixCurve;
        setParameters(rotationPrecision, n, k, a, b, c, d);
    }

    public void setParameters(int rotationPrecision, int n, int k, double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.n = n;
        this.k = k;
        this.rotationPrecision = rotationPrecision;
        if (generatrixCurve == null) return;
        if (generatrixCurve.isEmpty()) return;

        reProcessSpline();
        reProcessRotations();
        reProcessCircles();

        setObjectRotationMatrix(new Matrix3D(new double[]{
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1,
        }));
        updateObjectPoints();
    }

    private void updateObjectPoints() {
        objectPoints = new ArrayList<>();
        initAABB();
        updatePoints(actualPoints);
        updatePoints(circles);
    }

    private void updatePoints(ArrayList<ArrayList<Vector3D>> points) {
        Vector3D moveVector = new Vector3D(xMove, yMove, zMove);

        for (ArrayList<Vector3D> pointsArray : points) {
            ArrayList<Vector3D> rotatedLine = new ArrayList<>();
            for (Vector3D point : pointsArray) {
                Vector3D transformed = objectRotationMatrix.matMul(point).add(moveVector);
                aabb.checkPoints(transformed);
                rotatedLine.add(transformed);
            }
            objectPoints.add(rotatedLine);
        }
    }

    private void initAABB() {
        aabb = null;
        Vector3D moveVector = new Vector3D(xMove, yMove, zMove);
        if (actualPoints.isEmpty()) return;
        if (actualPoints.get(0).isEmpty()) return;
        aabb = new AABB(objectRotationMatrix.matMul(actualPoints.get(0).get(0)).add(moveVector));
    }

    public ArrayList<ArrayList<Vector3D>> getObjectPoints() {
        return objectPoints;
    }


    public void setRotationAngle(int x, int y, int z) {
        xRotationAngle = x;
        yRotationAngle = y;
        zRotationAngle = z;

        double rx = x * 2.0 * Math.PI / 360;
        double ry = y * 2.0 * Math.PI / 360;
        double rz = z * 2.0 * Math.PI / 360;

        setObjectRotationMatrix(Matrix3D.getRotationMatrix(rx, ry, rz));
    }

    private ArrayList<Vector3D> createCircle(Vector3D value3d) {
        ArrayList<Vector3D> circlePoints = new ArrayList<>();

        double rotationBegins = Math.toRadians(c);
        for (int i = 0; i < rotationPrecision; i++) {
            Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(rotationBegins + i * Math.toRadians(d - c) / rotationPrecision, 0, 0);
            circlePoints.add(rotationMatrix.matMul(value3d));
        }

        Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(Math.toRadians(d), 0, 0);
        circlePoints.add(rotationMatrix.matMul(value3d));

        return circlePoints;
    }

    private void reProcessCircles() {
        circles = new ArrayList<>();

        if (splinePoints3D.isEmpty()) return;
        circles.add(createCircle(splinePoints3D.get(0)));

        for (int i = k; i < splinePoints3D.size() - 1; i += k) {
            circles.add(createCircle(splinePoints3D.get(i)));
        }

        circles.add(createCircle(splinePoints3D.get(splinePoints3D.size() - 1)));
    }

    private void reProcessRotations() {
        actualPoints = new ArrayList<>();
        if (splinePoints3D.isEmpty()) return;
        double rotationBegins = Math.toRadians(c);
        for (int i = 0; i < rotationPrecision; i++) {
            Matrix3D rotationMatrix = Matrix3D.getRotationMatrix(rotationBegins + i * Math.toRadians(d - c) / rotationPrecision, 0, 0);

            ArrayList<Vector3D> points3dRotated = new ArrayList<>();
            for (Vector3D point3d : splinePoints3D) {
                Vector3D rotated = rotationMatrix.matMul(point3d);
                points3dRotated.add(rotated);
            }

            actualPoints.add(points3dRotated);
        }
    }

    private void reProcessSpline() {
        ArrayList<PointD> points2d = generatrixCurve.getCubicPoints(n, k, a, b); // <-- this not working correctly, idk why

        splinePoints3D = new ArrayList<>();
        for (PointD point2d : points2d) {
            splinePoints3D.add(new Vector3D(new double[]{point2d.x, point2d.y, 0}));
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public ArrayList<Integer> getAngles() {
        ArrayList<Integer> angles = new ArrayList<>();
        angles.add(xRotationAngle);
        angles.add(yRotationAngle);
        angles.add(zRotationAngle);
        return angles;
    }

    public Vector3D getCoords() {
        return new Vector3D(xMove, yMove, zMove);
    }

    public void setCoords(double x, double y, double z) {
        xMove = x;
        yMove = y;
        zMove = z;

        updateObjectPoints();
    }

    public BSpline2D getGeneratrixCurve() {
        return generatrixCurve;
    }

    public Matrix3D getObjectRotationMatrix() {
        return objectRotationMatrix;
    }

    public void setObjectRotationMatrix(Matrix3D objectRotationMatrix) {
        this.objectRotationMatrix = objectRotationMatrix;
        updateObjectPoints();
    }
}
