package com.nsu.bigguys.model.scene;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.model.linal.Vector3D;
import com.nsu.bigguys.model.objects3D.AABB;
import com.nsu.bigguys.model.objects3D.RevolutionSurface;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Scene implements Serializable {
    private static final int CANVAS_SIZE = 600;
    public int n = 5, k = 5, m = 10;
    public double a = 0.050000, b = 0.950000, c = 0, d = 180;
    public AABB maxAABB = new AABB();
    @Getter @Setter
    private double zf = 3.35;
    @Getter @Setter
    private double zb = 11.35;
    @Getter @Setter
    private double Sw = 0.005;
    @Getter @Setter
    double Sh = 0.005;
    private Cam camera;
    private ArrayList<Vector3D> xAxis;
    private ArrayList<Vector3D> yAxis;
    private ArrayList<Vector3D> zAxis;
    private Color color = Color.WHITE;
    private Matrix3D minMaxScaler;
    private Matrix3D mper;
    private Map<String, RevolutionSurface> objects3D = new HashMap<>();
    private double minX = 0;
    private double maxX = 0;
    private double minY = 0;
    private double maxY = 0;
    private double minZ = 0;
    private double maxZ = 0;
    private String selectedObj;

    public Scene(Cam camera) {
        setCamera(camera);
        createGrid();
        setPerspectiveMatrix();
        minMaxScale();
    }

    public Scene(Cam camera, double zn, double zf, double sh, double sw) {
        this.zf = zn;
        this.zb = zf;
        double maxS = Math.max(sw, sh);
        Sh = CANVAS_SIZE * 0.00001 * sh / maxS;
        Sw = CANVAS_SIZE * 0.00001 * sw / maxS;

        setCamera(camera);
        createGrid();
        setPerspectiveMatrix();
        minMaxScale();
    }

    public void setParameters(double a, double b, double c, double d,
                              int n, int m, int k,
                              double zn, double sw, double sh) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.n = n;
        this.m = m;
        this.k = k;
        this.zf = zn;
        this.zb = zn;
        double maxS = Math.max(sw, sh);
        Sh = CANVAS_SIZE * 0.00001 * sh / maxS;
        Sw = CANVAS_SIZE * 0.00001 * sw / maxS;

        for (RevolutionSurface object : objects3D.values()) {
            object.setParameters(m, n, k, a, b, c, d);
        }
        minMaxScale();
    }

    // TODO: extract graphics to view
    public void render(Graphics g) {
        Color prevColor = g.getColor();
        g.setColor(this.color);
        g.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE);

        g.setColor(Color.LIGHT_GRAY);

        g.setColor(Color.GREEN);
        drawBox(g);
        ((Graphics2D) g).setStroke(new BasicStroke(3));
        drawLine(yAxis.get(0), yAxis.get(1), g);
        g.setColor(Color.RED);
        drawLine(xAxis.get(0), xAxis.get(1), g);
        drawLine(zAxis.get(0), zAxis.get(1), g);
        ((Graphics2D) g).setStroke(new BasicStroke(1));

        g.setColor(prevColor);

        drawSelected(g);

        for (RevolutionSurface object : objects3D.values()) {
            drawObject(object, g);
        }
    }

    // TODO: extract graphics to view
    private void drawLine(Vector3D begin, Vector3D end, Graphics g) {
        begin = camera.getTransformationMatrix().matMul(begin);
        begin = begin.mul(1.0 / begin.getData()[3]);

        end = camera.getTransformationMatrix().matMul(end);
        end = end.mul(1.0 / end.getData()[3]);

        begin = mper.matMul(begin);
        end = mper.matMul(end);

        Point first = new Point(
                (int) begin.getX(),
                (int) begin.getY()
        );

        Point second = new Point(
                (int) end.getX(),
                (int) end.getY()
        );

        g.drawLine(
                first.x + 300,
                first.y + 300,
                second.x + 300,
                second.y + 300
        );
    }

    // TODO: extract graphics to view
    private void drawBox(Graphics g) {
        ArrayList<ArrayList<Vector3D>> lines = new ArrayList<>();

        double[] X = new double[2];
        X[0] = -xAxis.get(1).getX();
        X[1] = xAxis.get(1).getX();

        double[] Y = new double[2];
        Y[0] = -yAxis.get(1).getY();
        Y[1] = yAxis.get(1).getY();

        double[] Z = new double[2];
        Z[0] = -zAxis.get(1).getZ();
        Z[1] = zAxis.get(1).getZ();

        createBox(lines, X, Y, Z);

        for (ArrayList<Vector3D> line1 : lines) {
            if (line1.isEmpty()) continue;
            for (int i = 1; i < line1.size(); i++) {
                drawLine(line1.get(i - 1), line1.get(i), g);
            }
        }
    }

    // TODO: extract graphics to view
    private void createGrid() {
        ArrayList<ArrayList<Vector3D>> grid = new ArrayList<>();
        for (int i = -10; i < 11; i += 1) {
            ArrayList<Vector3D> yline = new ArrayList<>();
            Vector3D begin = new Vector3D(1, 0, (double) i / 10);
            Vector3D end = new Vector3D(-1, 0, (double) i / 10);
            yline.add(begin);
            yline.add(end);

            ArrayList<Vector3D> xline = new ArrayList<>();
            begin = new Vector3D((double) i / 10, 0, 1);
            end = new Vector3D((double) i / 10, 0, -1);
            xline.add(begin);
            xline.add(end);

            grid.add(yline);
            grid.add(xline);
        }

        xAxis = new ArrayList<>();
        yAxis = new ArrayList<>();
        zAxis = new ArrayList<>();

        xAxis.add(new Vector3D(0, 0, 0));
        xAxis.add(new Vector3D(1, 0, 0));

        yAxis.add(new Vector3D(0, 0, 0));
        yAxis.add(new Vector3D(0, 1, 0));

        zAxis.add(new Vector3D(0, 0, 0));
        zAxis.add(new Vector3D(0, 0, 1));
    }

    public void setCamera(Cam camera) {
        this.camera = camera;
    }

    public void addObject(RevolutionSurface object, String name) {
        objects3D.put(name, object);
        checkMaxAABB(object.aabb);
        if (objects3D.size() == 1) {
            minMaxScale();
        } else {
            reCalcMinMax(object);
        }
    }

    private void checkMaxAABB(AABB aabb) {
        if (aabb == null) return;
        maxAABB.checkPoints(aabb.minValues);
        maxAABB.checkPoints(aabb.maxValues);
    }

    private void setPerspectiveMatrix() {
        double[] perspectiveData = new double[]{
                2 * zf / Sw, 0, 0, 0,
                0, 2 * zf / Sh, 0, 0,
                0, 0, 1 / (zb - zf), -zb * zf / (zb - zf),
                0, 0, 1, 0
        };
        mper = new Matrix3D(perspectiveData);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    private void updateScales(AABB aabb) {
        if (aabb == null) return;
        if (aabb.minValues.getX() < minX) minX = aabb.minValues.getX();
        if (aabb.maxValues.getX() > maxX) maxX = aabb.maxValues.getX();

        if (aabb.minValues.getY() < minY) minY = aabb.minValues.getY();
        if (aabb.maxValues.getY() > maxY) maxY = aabb.maxValues.getY();

        if (aabb.minValues.getZ() < minZ) minZ = aabb.minValues.getZ();
        if (aabb.maxValues.getZ() > maxZ) maxZ = aabb.maxValues.getZ();
    }

    private void reCalcMinMax(RevolutionSurface object) {
        updateScales(object.aabb);

        createMinMaxMatrix();
    }

    private void createMinMaxMatrix() {
        double maxdifference = (maxX - minX);
        if ((maxY - minY) > maxdifference) maxdifference = maxY - minY;
        if ((maxZ - minZ) > maxdifference) maxdifference = maxZ - minZ;

        double shiftX = -(maxX - minX) / 2 - minX;
        double shiftY = -(maxY - minY) / 2 - minY;
        double shiftZ = -(maxZ - minZ) / 2 - minZ;

        minMaxScaler = new Matrix3D(new double[]{
                2 / maxdifference, 0, 0, 0,
                0, 2 / maxdifference, 0, 0,
                0, 0, 2 / maxdifference, 0,
                0, 0, 0, 1
        }).matMul(new Matrix3D(new double[]{
                1, 0, 0, shiftX,
                0, 1, 0, shiftY,
                0, 0, 1, shiftZ,
                0, 0, 0, 1,
        }));
    }

    private boolean zeroScales() {
        Vector3D point = null;
        if (!objects3D.isEmpty() &&
                !(new ArrayList<>(objects3D.values())).get(0).getObjectPoints().isEmpty() &&
                !(new ArrayList<>(objects3D.values()).get(0).getObjectPoints().get(0).isEmpty())) {
            point = new ArrayList<>(objects3D.values()).get(0).getObjectPoints().get(0).get(0);
        }

        if (point == null) return false;

        minX = point.getX();
        maxX = point.getX();
        minY = point.getY();
        maxY = point.getY();
        minZ = point.getZ();
        maxZ = point.getZ();

        return true;
    }

    private void minMaxScale() {
        if (!zeroScales()) {
            minMaxScaler = new Matrix3D(new double[]{
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1
            });

            return;
        }

        for (RevolutionSurface object : objects3D.values()) {
            updateScales(object.aabb);
        }

        createMinMaxMatrix();
    }

    // TODO: extract graphics to view
    private void drawObject(RevolutionSurface object, Graphics g) {
        Color prevColor = g.getColor();
        g.setColor(object.getColor());

        ArrayList<ArrayList<Vector3D>> lines = object.getObjectPoints();
        if (lines == null) return;

        drawLines(g, lines);

        g.setColor(prevColor);
    }

    // TODO: extract graphics to view
    private void drawSelected(Graphics g) {
        if (selectedObj != null) {
            ((Graphics2D) g).setStroke(new BasicStroke(2));
            drawObject(objects3D.get(selectedObj), g);
            ((Graphics2D) g).setStroke(new BasicStroke(1));

            AABB aabb = objects3D.get(selectedObj).aabb;
            if (aabb == null) return;
            ArrayList<ArrayList<Vector3D>> lines = new ArrayList<>();

            double[] X = new double[2];
            X[0] = aabb.minValues.getX();
            X[1] = aabb.maxValues.getX();

            double[] Y = new double[2];
            Y[0] = aabb.minValues.getY();
            Y[1] = aabb.maxValues.getY();

            double[] Z = new double[2];
            Z[0] = aabb.minValues.getZ();
            Z[1] = aabb.maxValues.getZ();

            createBox(lines, X, Y, Z);

            drawLines(g, lines);
        }
    }

    // TODO: extract graphics to view
    private void drawLines(Graphics g, ArrayList<ArrayList<Vector3D>> lines) {
        for (ArrayList<Vector3D> line1 : lines) {
            if (line1.isEmpty()) continue;
            for (int i = 1; i < line1.size(); i++) {
                drawLine(minMaxScaler.matMul(line1.get(i - 1)), minMaxScaler.matMul(line1.get(i)), g);
            }
        }
    }

    // TODO: extract graphics to view
    private void createBox(ArrayList<ArrayList<Vector3D>> lines, double[] x, double[] y, double[] z) {
        ArrayList<Vector3D> line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[0], z[0]));
        line.add(new Vector3D(x[1], y[0], z[0]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[0], z[0]));
        line.add(new Vector3D(x[0], y[1], z[0]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[0], z[0]));
        line.add(new Vector3D(x[0], y[0], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[1], y[0], z[0]));
        line.add(new Vector3D(x[1], y[1], z[0]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[1], z[0]));
        line.add(new Vector3D(x[1], y[1], z[0]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[0], z[1]));
        line.add(new Vector3D(x[0], y[1], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[0], z[1]));
        line.add(new Vector3D(x[1], y[0], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[0], y[1], z[0]));
        line.add(new Vector3D(x[0], y[1], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[1], y[0], z[0]));
        line.add(new Vector3D(x[1], y[0], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[1], y[1], z[1]));
        line.add(new Vector3D(x[0], y[1], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[1], y[1], z[1]));
        line.add(new Vector3D(x[1], y[0], z[1]));

        lines.add(line);
        line = new ArrayList<>();

        line.add(new Vector3D(x[1], y[1], z[1]));
        line.add(new Vector3D(x[1], y[1], z[0]));

        lines.add(line);
    }

    public Set<String> getObejctsNames() {
        return objects3D.keySet();
    }

    public RevolutionSurface getObjectByName(String name) {
        return objects3D.get(name);
    }

    public void recompute() {
        minMaxScale();
    }

    public void setSelectedObj(String objName) {
        selectedObj = objName;
    }

    public void dropSelectedObj() {
        selectedObj = null;
    }

    public void placeObject(RevolutionSurface object, String name) {
        object.setCoords(maxX, maxY, maxZ);
        addObject(object, name);
    }
}
