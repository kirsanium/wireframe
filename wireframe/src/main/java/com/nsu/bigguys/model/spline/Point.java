package com.nsu.bigguys.model.spline;

import com.nsu.bigguys.view.splineEditor.SplineEditor;

import java.io.Serializable;
import java.util.ArrayList;

public class Point implements Serializable {
    public int x;
    public int y;

    int getX(){
        return x;
    }

    int getY(){
        return y;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point other) {
        this.x = other.x;
        this.y = other.y;
    }

    public static ArrayList<PointD> fromScreenPoints(ArrayList<Point> points) {
        ArrayList<PointD> d = new ArrayList<>();
        for (Point point : points) {
            d.add(new PointD(point.x - (double)(SplineEditor.canvasSize.width) / 2, point.y - (double)(SplineEditor.canvasSize.height) / 2));
        }
        return d;
    }

    public static ArrayList<Point> fromD(ArrayList<PointD> d) {
        ArrayList<Point> points = new ArrayList<>();
        for (PointD point : d) {
            points.add(new Point((int) point.x, (int) point.y));
        }
        return points;
    }
}
