package com.nsu.bigguys.model.objects3D;

import com.nsu.bigguys.model.linal.Vector3D;

import java.io.Serializable;

/**
 * Axis-aligned bounding box
 */
public class AABB implements Serializable {
    public Vector3D minValues;
    public Vector3D maxValues;

    public AABB() {
        this.minValues = new Vector3D(0, 0, 0);
        this.maxValues = new Vector3D(0, 0, 0);
    }

    public AABB(Vector3D values) {
        minValues = new Vector3D(values);
        maxValues = new Vector3D(values);
    }

    public void checkPoints(Vector3D points) {
        checkMinX(points.getX());
        checkMaxX(points.getX());
        checkMinY(points.getY());
        checkMaxY(points.getY());
        checkMinZ(points.getZ());
        checkMaxZ(points.getZ());
    }

    private void checkMinX(double val) {
        if (minValues.getX() > val) {
            minValues.setX(val);
        }
    }

    private void checkMinY(double val) {
        if (minValues.getY() > val) {
            minValues.setY(val);
        }
    }

    private void checkMinZ(double val) {
        if (minValues.getZ() > val) {
            minValues.setZ(val);
        }
    }

    private void checkMaxX(double val) {
        if (maxValues.getX() < val) {
            maxValues.setX(val);
        }
    }

    private void checkMaxY(double val) {
        if (maxValues.getY() < val) {
            maxValues.setY(val);
        }
    }

    private void checkMaxZ(double val) {
        if (maxValues.getZ() < val) {
            maxValues.setZ(val);
        }
    }
}
