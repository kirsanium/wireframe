package com.nsu.bigguys.model.scene;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.model.linal.Vector3D;
import lombok.Getter;

import java.io.Serializable;

public class Cam implements Serializable {
    @Getter
    private Vector3D location;
    @Getter
    private Vector3D look_at;

    private double[] aspect_ratio = new double[2];

    private Matrix3D transformationMatrix;

    public Cam(Vector3D location, Vector3D look_at) {
        aspect_ratio[0] = 3;
        aspect_ratio[1] = 2;

        this.location = location;
        this.look_at = look_at;

        setTransformationMatrix();
    }

    public Cam(double location_x,
               double location_y,
               double location_z,

               double look_at_x,
               double look_at_y,
               double look_at_z) {

        aspect_ratio[0] = 3;
        aspect_ratio[1] = 2;

        this.location = new Vector3D(location_x, location_y, location_z);
        this.look_at = new Vector3D(look_at_x, look_at_y, look_at_z);

        setTransformationMatrix();
    }

    private void setTransformationMatrix() {
        Vector3D relationalVec = this.location.subtract(this.look_at);
        relationalVec.getData()[3] = 1;

        Vector3D w = relationalVec.normalized().mul(1);
        Vector3D Vup = new Vector3D(0, 1, 0);
        Vector3D u = Vup.crossProduct(w).normalized();
        Vector3D v = w.crossProduct(u);


        double[] rotationData = new double[4 * 4];

        for (int i = 0; i < 3; i++) {
            rotationData[i * 4 + 3] = 0;
            rotationData[3 * 4 + i] = 0;

            rotationData[0 * 4 + i] = u.getData()[i];
            rotationData[1 * 4 + i] = v.getData()[i];
            rotationData[2 * 4 + i] = w.getData()[i];
        }

        rotationData[4 * 4 - 1] = 1;

        transformationMatrix = new Matrix3D(rotationData);

        double[] moveData = new double[4 * 4];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                moveData[4 * i + j] = i == j ? 1 : 0;
            }
        }

        for (int i = 0; i < 3; i++) {
            moveData[i * 4 + 3] = -this.location.getData()[i];
        }

        moveData[4 * 4 - 1] = 1;

        Matrix3D moveMatrix = new Matrix3D(moveData);
        transformationMatrix = transformationMatrix.matMul(moveMatrix);
    }

    public Matrix3D getTransformationMatrix() {
        return transformationMatrix;
    }

}
