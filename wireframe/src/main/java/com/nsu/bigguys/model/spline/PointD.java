package com.nsu.bigguys.model.spline;

import java.io.Serializable;

public class PointD implements Serializable {
    public double x;
    public double y;

    public PointD(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
