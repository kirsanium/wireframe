package com.nsu.bigguys.model.linal;

import java.io.Serializable;

public class MatrixND implements Serializable {
    private double[] data;

    private int n;
    private int m;

    public MatrixND(double[] data, int n, int m) {
        this.data = data;

        reshape(n, m);
    }

    private void reshape(int n, int m) {
        assert n > 0 && m > 0;
        assert n * m == data.length;
        this.n = n;
        this.m = m;
    }

    public VectorND matMul(VectorND vec) {
        assert m == vec.getN();

        double[] vecData = new double[n];

        for (int i = 0; i < n; i++) {
            vecData[i] = 0;
        }

        for (int i = 0; i < n; i++) {
            for (int k = 0; k < m; k++) {
                vecData[i] += _get(i, k) * vec.get(k);
            }
        }

        return new VectorND(vecData, n);
    }

    public double[] getData() { return data; }

    int getN() { return n; }

    int getM() { return m; }

    public double get(int x, int y) {
        assert x >= 0 && x < n;
        assert y >= 0 && y < m;

        return _get(x, y);
    }

    public MatrixND div(double coefficient) {
        double[] divData = new double[n * m];
        for (int i = 0; i < m * n; i++) {
            divData[i] = data[i] / coefficient;
        }

        return new MatrixND(divData, n, m);
    }

    private double _get(int x, int y) {
        return data[x * m + y];
    }
}
