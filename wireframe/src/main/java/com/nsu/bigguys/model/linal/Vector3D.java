package com.nsu.bigguys.model.linal;

import java.io.Serializable;
import java.security.InvalidParameterException;

public class Vector3D {
    //odnorodny
    private static final int N_DIMS = 4;
    private double[] data;

    public Vector3D(Vector3D other) {
        data = new double[N_DIMS];
        System.arraycopy(other.data, 0, data, 0, N_DIMS);
    }

    public Vector3D(double[] data) {
        if (data.length == N_DIMS - 1) {
            this.data = new double[N_DIMS];

            System.arraycopy(data, 0, this.data, 0, N_DIMS - 1);
            this.data[N_DIMS - 1] = 1;

            return;
        }
        if (data.length != N_DIMS) {
            throw new InvalidParameterException("wrong vector data length");
        }

        this.data = data;
    }

    public Vector3D(double x, double y, double z) {
        data = new double[N_DIMS];
        data[0] = x;
        data[1] = y;
        data[2] = z;
        data[3] = 1;
    }

    public Vector3D mul(double coef) {
        double[] mulData = new double[N_DIMS];

        mulData[0] = data[0] * coef;
        mulData[1] = data[1] * coef;
        mulData[2] = data[2] * coef;
        mulData[3] = data[3] * coef;

        return new Vector3D(mulData);
    }

    public Vector3D crossProduct(Vector3D other) {
        return new Vector3D(new double[]{
                data[1] * other.data[2] - data[2] * other.data[1],
                data[2] * other.data[0] - data[0] * other.data[2],
                data[0] * other.data[1] - data[1] * other.data[0]
        });
    }

    public Vector3D subtract(Vector3D other) {
        double[] subData = new double[N_DIMS];

        for (int i = 0; i < N_DIMS - 1; i++) {
            subData[i] = data[i] - other.data[i];
        }

        data[N_DIMS - 1] = 1;

        return new Vector3D(subData);
    }

    public Vector3D add(Vector3D other) {
        double[] subData = new double[N_DIMS];

        for (int i = 0; i < N_DIMS - 1; i++) {
            subData[i] = data[i] + other.data[i];
        }

        subData[N_DIMS - 1] = data[N_DIMS - 1];

        return new Vector3D(subData);

    }

    public double norm() {
        return Math.sqrt(data[0] * data[0] + data[1] * data[1] + data[2] * data[2]);
    }

    public void normalize() {
        double norm = norm();

        if (norm != 0) {
            data[0] /= norm;
            data[1] /= norm;
            data[2] /= norm;
            data[3] = 1;
        }
    }

    public Vector3D normalized() {
        double[] normalizedData = new double[]{data[0], data[1], data[2]};
        Vector3D normalized = new Vector3D(normalizedData);
        normalized.normalize();
        return normalized;
    }

    public double[] getData() {
        return data;
    }

    public double getX() { return data[0]; }

    public void setX(double x) { data[0] = x; }

    public double getY() { return data[1]; }

    public void setY(double y) {data[1] = y; }

    public double getZ() { return data[2]; }

    public void setZ(double z) { data[2] = z; }
}
