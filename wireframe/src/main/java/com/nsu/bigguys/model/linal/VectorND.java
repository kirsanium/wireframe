package com.nsu.bigguys.model.linal;

import java.io.Serializable;

public class VectorND implements Serializable {
    private int n;

    private double[] data;

    public VectorND(double[] data, int n) {
        assert data.length <= n;

        this.n = n;
        this.data = data.clone();
    }

    public VectorND(double[] data) {
        this.n = data.length;
        this.data = data.clone();
    }

    public double get(int index) {
        assert index >= 0 && index < n;

        return data[index];
    }

    //returns n
    public int getN() {
        return n;
    }

    public double[] getData() {
        return data;
    }
}
