package com.nsu.bigguys.model.linal;

import java.io.Serializable;
import java.security.InvalidParameterException;

public class Matrix3D implements Serializable {
    private static final int N_DIMS = 4;
    private double[] data;

    public Matrix3D(double[] data) {
        if (data.length == (N_DIMS - 1) * (N_DIMS - 1)) {
            this.data = new double[N_DIMS * N_DIMS];
            for (int i = 0; i < N_DIMS - 1; i++) {
                this.data[i * (N_DIMS) + N_DIMS - 1] = 0;
                this.data[(N_DIMS - 1) * (N_DIMS) + i] = 0;
                System.arraycopy(data, i * 3, this.data, i * 4, N_DIMS - 1);
            }

            this.data[this.data.length - 1] = 1;
            return;
        }
        if (data.length != N_DIMS * N_DIMS) {
            throw new InvalidParameterException("invalid matrix data length");
        }
        this.data = data;
    }

    static public Matrix3D getRotationMatrix(double ax, double ay, double az) {
        double[] RxData = {
                1, 0e1, 0e1,
                0e0, Math.cos(ax), -Math.sin(ax),
                0e0, Math.sin(ax), Math.cos(ax)
        };

        double[] RyData = {
                Math.cos(ay), 0, Math.sin(ay),
                0, 1, 0,
                -Math.sin(ay), 0, Math.cos(ay)
        };

        double[] RzData = {
                Math.cos(az), -Math.sin(az), 0,
                Math.sin(az), Math.cos(az), 0,
                0, 0, 1
        };

        return new Matrix3D(RxData).matMul(new Matrix3D(RyData)).matMul(new Matrix3D(RzData));
    }

    public double[] getData() {
        return data;
    }

    public Matrix3D matMul(Matrix3D other) {
        double[] mulData = new double[N_DIMS * N_DIMS];
        for (int i = 0; i < N_DIMS; i++) {
            mulData[i] = 0;
        }

        for (int i = 0; i < N_DIMS; i++) {
            for (int j = 0; j < N_DIMS; j++) {
                for (int k = 0; k < N_DIMS; k++) {
                    mulData[i * N_DIMS + j] += data[i * N_DIMS + k] * other.data[k * N_DIMS + j];
                }
            }
        }

        return new Matrix3D(mulData);
    }
    //multiply this vector with vec
    public Vector3D matMul(Vector3D vec) {
        double[] vecData = new double[N_DIMS];
        for (int i = 0; i < N_DIMS; i++) {
            vecData[i] = 0;
        }

        for (int i = 0; i < N_DIMS; i++) {
            for (int k = 0; k < N_DIMS; k++) {
                vecData[i] += data[i * N_DIMS + k] * vec.getData()[k];
            }
        }

        for (int i = 0; i < N_DIMS; i++) {
            vecData[i] /= vecData[N_DIMS - 1];
        }

        return new Vector3D(vecData);
    }
}
