package com.nsu.bigguys.model.spline;

import com.nsu.bigguys.model.linal.MatrixND;
import com.nsu.bigguys.model.linal.VectorND;
import com.nsu.bigguys.view.splineEditor.SplineEditor;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public class BSpline2D implements Serializable {
    private static final MatrixND BASIS_M = new MatrixND(new double[]{
            -1, 3, -3, 1,
            3, -6, 3, 0,
            -3, 0, 3, 0,
            1, 4, 1, 0
    }, 4, 4).div(6);
    public double scale = 1.;
    private VectorND xvec;
    private VectorND yvec;
    private ArrayList<PointD> controlpoints;

    public BSpline2D(ArrayList<PointD> controlpoints) {
        this.controlpoints = new ArrayList<>();
        for (PointD point : controlpoints) {
            this.controlpoints.add(new PointD(point.x, point.y));
        }
    }

    public BSpline2D(PointD[] controlpoints) {
        this.controlpoints = new ArrayList<>();
        for (PointD point : controlpoints) {
            this.controlpoints.add(new PointD(point.x, point.y));
        }
    }

    public BSpline2D(int[] xcontrolpoints, int[] ycontrolpoints) {
        assert xcontrolpoints.length == ycontrolpoints.length;
        int length = xcontrolpoints.length;

        this.controlpoints = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            this.controlpoints.add(new PointD(xcontrolpoints[i], ycontrolpoints[i]));
        }
    }

    public ArrayList<PointD> getControlPoints() {
        return controlpoints;
    }

    double Polynomial(VectorND vec, double val) {
        double result = 0.f;
        double bias = 1.f;

        int length = vec.getN();

        for (int i = 0; i < length; i++) {
            result += vec.get(length - i - 1) * bias;
            bias *= val;
        }

        return result;
    }

    VectorND getSliceX(int begin, int end) {
        assert begin >= 0;
        assert end > begin;
        assert end <= controlpoints.size();

        double[] sliceData = new double[end - begin];

        for (int i = 0; i < sliceData.length; i++) {
            sliceData[i] = controlpoints.get(begin + i).x;
        }

        return new VectorND(sliceData);
    }

    VectorND getSliceY(int begin, int end) {
        assert begin >= 0;
        assert end > begin;
        assert end <= controlpoints.size();

        double[] sliceData = new double[end - begin];

        for (int i = 0; i < sliceData.length; i++) {
            sliceData[i] = controlpoints.get(begin + i).y;
        }

        return new VectorND(sliceData);
    }

    public boolean isEmpty() {
        return controlpoints.isEmpty();
    }

    public ArrayList<PointD> getCubicPoints(double precision_step) {
        ArrayList<PointD> cubicPoints = new ArrayList<>();
        if (precision_step == 0) return cubicPoints;


        for (int i = 1; i < controlpoints.size() - 2; i++) {
            xvec = BASIS_M.matMul(getSliceX(i - 1, i + 3));
            yvec = BASIS_M.matMul(getSliceY(i - 1, i + 3));

            double cur;
            for (cur = 0; cur <= 1.0; cur += precision_step) {
                double x = Polynomial(xvec, cur);
                double y = Polynomial(yvec, cur);
                cubicPoints.add(new PointD(scale * x, scale * y));
            }
            if (cur - precision_step < 1.0) {
                double x = Polynomial(xvec, 1.0);
                double y = Polynomial(yvec, 1.0);
                cubicPoints.add(new PointD(scale * x, scale * y));
            }
        }

        return cubicPoints;
    }

    void drawCurveSector(double begin, double end, Graphics g) {
        int x0 = (int) Polynomial(xvec, begin);
        int x1 = (int) Polynomial(xvec, end);

        int y0 = (int) Polynomial(yvec, begin);
        int y1 = (int) Polynomial(yvec, end);

        g.drawLine(
                x0 + SplineEditor.canvasSize.width / 2,
                y0 + SplineEditor.canvasSize.height / 2,
                x1 + SplineEditor.canvasSize.width / 2,
                y1 + SplineEditor.canvasSize.height / 2);
    }

    public double getLength(double precision_step) {
        double len = 0;
        ArrayList<Point> cubicPoints = new ArrayList<>();
        if (precision_step == 0) return len;


        getCubicPoints(precision_step, cubicPoints);

        for (int i = 0; i < cubicPoints.size() - 1; i++) {
            len += SplineEditor.euclidianMetric(cubicPoints.get(i), cubicPoints.get(i + 1));
        }

        return len;
    }

    /**
     * TODO: optimize it
     *
     * @param t              relative length (from 0.0 to 1.0)
     * @param precision_step accuracy
     * @return spline value at t
     */
    public Point getValue(double t, double precision_step) {
        assert precision_step != 0;
        if (t < 0) t = 0;
        if (t > 1) t = 1;

        double total = getLength(precision_step);

        double len = 0;
        ArrayList<Point> cubicPoints = new ArrayList<>();
        getCubicPoints(precision_step, cubicPoints);

        for (int i = 0; i < cubicPoints.size() - 1; i++) {
            if (len / total >= t) return cubicPoints.get(i);
            len += SplineEditor.euclidianMetric(cubicPoints.get(i), cubicPoints.get(i + 1));
        }
        if (cubicPoints.isEmpty()) return null;
        return cubicPoints.get(cubicPoints.size() - 1);
    }

    private void getCubicPoints(double precision_step, ArrayList<Point> cubicPoints) {
        for (int i = 1; i < controlpoints.size() - 2; i++) {
            xvec = BASIS_M.matMul(getSliceX(i - 1, i + 3));
            yvec = BASIS_M.matMul(getSliceY(i - 1, i + 3));

            double cur;
            for (cur = 0; cur <= 1.0; cur += precision_step) {
                int x = (int) Polynomial(xvec, cur);
                int y = (int) Polynomial(yvec, cur);
                cubicPoints.add(new Point(x, y));
            }
            if (cur - precision_step < 1.0) {
                int x = (int) Polynomial(xvec, 1.0);
                int y = (int) Polynomial(yvec, 1.0);
                cubicPoints.add(new Point(x, y));
            }
        }
    }

    public void drawCubic(Graphics graphics, double precision_step) {
        if (precision_step == 0) return;

        for (int i = 1; i < controlpoints.size() - 2; i++) {
            xvec = BASIS_M.matMul(getSliceX(i - 1, i + 3));
            yvec = BASIS_M.matMul(getSliceY(i - 1, i + 3));

            double prev = 0;
            for (double cur = precision_step; cur <= 1.0; cur += precision_step) {
                drawCurveSector(prev, cur, graphics);
                prev = cur;
            }
            if (prev < 1.0) {
                drawCurveSector(prev, 1.0, graphics);
            }
        }
    }

    public ArrayList<PointD> getCubicPoints(int n, int k, double a, double b) {
        ArrayList<PointD> cubicPoints = new ArrayList<>();
        if (controlpoints.size() < 4) return cubicPoints;
        if (a > 1 || a < 0) return cubicPoints;
        if (b > 1 || b < 0) return cubicPoints;
        if (b < a) return cubicPoints;
        if (n < 1) return cubicPoints;
        if (k < 1) return cubicPoints;

        double precision = 1.0 / n / k / 100;
        if (precision == 0.0) return cubicPoints;

        xvec = BASIS_M.matMul(getSliceX(0, 4));
        yvec = BASIS_M.matMul(getSliceY(0, 4));

        var state = new CubicCalculationState(n, k, a, b, precision);

        for (int i = 1; i < controlpoints.size() - 2; i++) {
            xvec = BASIS_M.matMul(getSliceX(i - 1, i + 3));
            yvec = BASIS_M.matMul(getSliceY(i - 1, i + 3));
            double cur;
            for (cur = 0; cur < 1.0; cur += precision) {
               state.calculateCubicLocally(cubicPoints, cur);
            }
            if (cur - precision != 1.0) {
                state.calculateCubicLocally(cubicPoints, 1.0);
            }
        }

        return cubicPoints;
    }

    class CubicCalculationState {
        double a;
        double b;
        double length;
        double curLength;
        double curStep;
        double x0;
        double y0;
        double step;

        CubicCalculationState(int n, int k, double a, double b, double precision) {
            this.a = a;
            this.b = b;
            length = getLength(precision);
            step = length * (b - a) / n / k;
            curLength = 0;
            curStep = step;

            x0 = Polynomial(xvec, 0);
            y0 = Polynomial(yvec, 0);
        }

        private void calculateCubicLocally(ArrayList<PointD> cubicPoints,
                                           double cur) {
            double x = Polynomial(xvec, cur);
            double y = Polynomial(yvec, cur);
            curLength += Math.sqrt((x0 - x) * (x0 - x) + (y0 - y) * (y0 - y));
            x0 = x;
            y0 = y;
            if (curLength / length < a) return;
            if (curLength / length > b) return;
            if (curLength > curStep) {
                cubicPoints.add(new PointD(scale * x, scale * y));
                curStep += step;
            }
        }
    }

}
