package com.nsu.bigguys;

import com.nsu.bigguys.io.logging;
import com.nsu.bigguys.view.ScenePreview;

import javax.swing.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Wireframe extends JFrame {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            Logger.getLogger(logging.PROJECT_LOGGER_NAME).log(Level.FINER, e.toString());
            e.printStackTrace();
        }

        javax.swing.SwingUtilities.invokeLater(() -> {
            ScenePreview mainFrame = new ScenePreview();
            mainFrame.setVisible(true);
        });
    }
}
