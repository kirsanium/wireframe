package com.nsu.bigguys.io;

import com.nsu.bigguys.model.linal.Matrix3D;
import com.nsu.bigguys.model.objects3D.RevolutionSurface;
import com.nsu.bigguys.model.scene.Cam;
import com.nsu.bigguys.model.scene.Scene;
import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.PointD;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SceneSaveOpenUtils {
    private static final String UNEXPECTED_EOF_MESSAGE = "Unexpected EOF";

    private SceneSaveOpenUtils() {
        // hide implicit constructor
    }

    static public Scene openScene(File file) {
        if (file == null) {
            return null;
        }

        try (var bufferedReader = new BufferedReader(new FileReader(file))) {
            String[] tokens = getTokens(bufferedReader, 7);

            int n = Integer.parseInt(tokens[0]);
            int m = Integer.parseInt(tokens[1]);
            int k = Integer.parseInt(tokens[2]);

            double a = Double.parseDouble(tokens[3]);
            double b = Double.parseDouble(tokens[4]);
            double c = Double.parseDouble(tokens[5]);
            double d = Double.parseDouble(tokens[6]);

            tokens = getTokens(bufferedReader, 4);

            double zn = Double.parseDouble(tokens[0]);
            double zf = Double.parseDouble(tokens[1]);
            double sw = Double.parseDouble(tokens[2]);
            double sh = Double.parseDouble(tokens[3]);

            // FIXME: scene rotation matrix ignored
            double[] E = new double[9];

            tokens = getTokens(bufferedReader, 3);

            E[0] = Double.parseDouble(tokens[0]);
            E[1] = Double.parseDouble(tokens[1]);
            E[2] = Double.parseDouble(tokens[2]);

            tokens = getTokens(bufferedReader, 3);

            E[3] = Double.parseDouble(tokens[0]);
            E[4] = Double.parseDouble(tokens[1]);
            E[5] = Double.parseDouble(tokens[2]);

            tokens = getTokens(bufferedReader, 3);

            E[6] = Double.parseDouble(tokens[0]);
            E[7] = Double.parseDouble(tokens[1]);
            E[8] = Double.parseDouble(tokens[2]);

            tokens = getTokens(bufferedReader, 3);

            int BR = Integer.parseInt(tokens[0]);
            int BG = Integer.parseInt(tokens[1]);
            int BB = Integer.parseInt(tokens[2]);

            tokens = getTokens(bufferedReader, 1);

            Cam cam = new Cam(
                    -10, 0, 0,
                    10, 0, 0
            );
            var scene = new Scene(cam, zn, zf, sh, sw);
            scene.setColor(new Color(BR, BG, BB));
            scene.setParameters(a, b, c, d, n, m, k, zn, sw, sh);

            int K = Integer.parseInt(tokens[0]);

            for (int i = 0; i < K; i++) {
                tokens = getTokens(bufferedReader, 3);

                int R = Integer.parseInt(tokens[0]);
                int G = Integer.parseInt(tokens[1]);
                int B = Integer.parseInt(tokens[2]);

                tokens = getTokens(bufferedReader, 3);

                double CX = Double.parseDouble(tokens[0]);
                double CY = Double.parseDouble(tokens[1]);
                double CZ = Double.parseDouble(tokens[2]);

                tokens = getTokens(bufferedReader, 3);

                double[] rotationData = new double[9];

                rotationData[0] = Double.parseDouble(tokens[0]);
                rotationData[1] = Double.parseDouble(tokens[1]);
                rotationData[2] = Double.parseDouble(tokens[2]);

                tokens = getTokens(bufferedReader, 3);

                rotationData[3] = Double.parseDouble(tokens[0]);
                rotationData[4] = Double.parseDouble(tokens[1]);
                rotationData[5] = Double.parseDouble(tokens[2]);

                tokens = getTokens(bufferedReader, 3);

                rotationData[6] = Double.parseDouble(tokens[0]);
                rotationData[7] = Double.parseDouble(tokens[1]);
                rotationData[8] = Double.parseDouble(tokens[2]);

                tokens = getTokens(bufferedReader, 1);

                int N = Integer.parseInt(tokens[0]);

                var controlpoints = new ArrayList<PointD>();

                for (int j = 0; j < N; j++) {
                    tokens = getTokens(bufferedReader, 2);
                    double x = Double.parseDouble(tokens[0]);
                    double y = Double.parseDouble(tokens[1]);
                    controlpoints.add(new PointD(x, y));
                }

                RevolutionSurface object = new RevolutionSurface(new BSpline2D(controlpoints), m, n, k, a, b, c, d);
                object.setColor(new Color(R, G, B));
                object.setObjectRotationMatrix(new Matrix3D(rotationData));
                object.setCoords(CX, CY, CZ);

                String name = "Object #" + i;
                scene.addObject(object, name);
            }
            return scene;
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "file not found");
        } catch (IncorrectFileException e) {
            String description = e.getDescription();
            String message = "incorrect file";
            if (description != null) {
                message = message + ": " + description;
            }
            JOptionPane.showMessageDialog(null, message);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "wrong number format");
        } catch (IOException e) {
            Logger.getLogger(logging.PROJECT_LOGGER_NAME).log(Level.FINER, e.toString());
        }

        return null;
    }

    static public void saveScene(File file, Scene scene) {
        if (file == null) return;
        try (var bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(scene.n + " " + scene.m + " " + scene.k);
            bufferedWriter.write(" ");
            bufferedWriter.write(scene.a + " " + scene.b + " " + scene.c + " " + scene.d);
            bufferedWriter.newLine();
           // bufferedWriter.write(scene.getZb() + " " + scene.getZf() + " " + scene.getSw() + " " + scene.getSh());
            bufferedWriter.newLine();

            // FIXME: scene rotation matrix ignored
            var rotationMatrix = new Matrix3D(new double[]{
                    1e1, 0e1, 0e1,
                    0, 1, 0,
                    0, 0, 1
            });

            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    bufferedWriter.write(rotationMatrix.getData()[i * 4 + j] + " ");
                }
                bufferedWriter.newLine();
            }

            var sceneColor = scene.getColor();

            bufferedWriter.write(sceneColor.getRed() + " " + sceneColor.getGreen() + " " + sceneColor.getBlue());
            bufferedWriter.newLine();

            int objectsCount = scene.getObejctsNames().size();

            bufferedWriter.write(Integer.toString(objectsCount));
            bufferedWriter.newLine();

            for (String name : scene.getObejctsNames()) {
                RevolutionSurface object = scene.getObjectByName(name);

                var objColor = object.getColor();
                bufferedWriter.write(objColor.getRed() + " " + objColor.getGreen() + " " + objColor.getBlue());
                bufferedWriter.newLine();
                var coords = object.getCoords();
                bufferedWriter.write(coords.getX() + " " + coords.getY() + " " + coords.getZ());
                bufferedWriter.newLine();
                var objectRotationMatrix = object.getObjectRotationMatrix();

                for (int i = 0; i < 3; ++i) {
                    for (int j = 0; j < 3; ++j) {
                        bufferedWriter.write(objectRotationMatrix.getData()[i * 4 + j] + " ");
                    }
                    bufferedWriter.newLine();
                }

                var curve = object.getGeneratrixCurve().getControlPoints();

                bufferedWriter.write(Integer.toString(curve.size()));
                bufferedWriter.newLine();

                for (PointD pointD : curve) {
                    bufferedWriter.write(pointD.x + " " + pointD.y);
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            Logger.getLogger(logging.PROJECT_LOGGER_NAME).log(Level.FINER, e.toString());
        }
    }


    private static String[] getTokens(BufferedReader reader, int tokenLen) throws IncorrectFileException {
        String line = getUncommentedLine(reader);
        String[] tokens = line.trim().split("\\s+");

        if (tokens.length != tokenLen) {
            throw new IncorrectFileException("wrong tokens num");
        }

        return tokens;
    }


    private static String getUncommentedLine(BufferedReader reader) throws IncorrectFileException {
        try {
            String line = reader.readLine();
            if (line == null) {
                throw new IncorrectFileException(UNEXPECTED_EOF_MESSAGE);
            }
            if (line.equals("")) {
                return "";
            }
            line = line.replaceFirst("//.*", "");
            while (line.equals("")) {
                line = reader.readLine();
                if (line == null) {
                    throw new IncorrectFileException(UNEXPECTED_EOF_MESSAGE);
                }
                if (line.equals("")) {
                    throw new IncorrectFileException(UNEXPECTED_EOF_MESSAGE);
                }

                line = line.replaceFirst("//.*", "");
            }
            return line;
        } catch (IOException e) {
            Logger.getLogger("Wireframe").log(Level.FINER, e.toString());

            throw new IncorrectFileException("unexpected EOF");
        }
    }
}
