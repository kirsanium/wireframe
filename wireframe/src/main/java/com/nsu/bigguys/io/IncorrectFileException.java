package com.nsu.bigguys.io;

public class IncorrectFileException extends Exception {
    private String description;

    public IncorrectFileException(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
