package com.nsu.bigguys.model.scene;

import com.nsu.bigguys.model.linal.Vector3D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class CamTest {

    @Test
    public void Should_ConstructCamFromVectors() {
        var expectedTransformationMatrixArray = new double[]{0,0,1,0,0,1,0,0,-1,0,0,0,0,0,0,1};

        var location = new Vector3D(new double[]{0,0,0});
        var lookAt = new Vector3D(new double[]{1,0,0});

        var cam = new Cam(location, lookAt);

        assertArrayEquals(location.getData(), cam.getLocation().getData());
        assertArrayEquals(lookAt.getData(), cam.getLook_at().getData());

        assertArrayEquals(expectedTransformationMatrixArray, cam.getTransformationMatrix().getData());
    }

    @Test
    public void Should_ConstructCamFromDoubles() {
        var expectedTransformationMatrixArray = new double[]{0,0,1,0,0,1,0,0,-1,0,0,0,0,0,0,1};

        var location = new Vector3D(new double[]{0,0,0});
        var lookAt = new Vector3D(new double[]{1,0,0});

        var cam = new Cam(0,0,0,1,0,0);

        assertArrayEquals(location.getData(), cam.getLocation().getData());
        assertArrayEquals(lookAt.getData(), cam.getLook_at().getData());

        assertArrayEquals(expectedTransformationMatrixArray, cam.getTransformationMatrix().getData());
    }

}
