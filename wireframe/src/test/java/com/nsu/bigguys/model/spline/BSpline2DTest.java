package com.nsu.bigguys.model.spline;

import com.nsu.bigguys.model.linal.VectorND;
import com.nsu.bigguys.SoftAssertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BSpline2DTest {
    @Test
    void Should_ConstructFromArrayListCorrectly() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(2, 1));
        controlpoints.add(new PointD(1000, -800));

        var spline = new BSpline2D(controlpoints);
        var splineControlPoints = spline.getControlPoints();

        assertEquals(2, splineControlPoints.size());
        assertEquals(2, splineControlPoints.get(0).x);
        assertEquals(1, splineControlPoints.get(0).y);
        assertEquals(1000, splineControlPoints.get(1).x);
        assertEquals(-800, splineControlPoints.get(1).y);
    }

    @Test
    void Should_ConstructFromJavaArrayCorrectly() {
        var controlpoints = new PointD[2];
        controlpoints[0] = new PointD(2, 1);
        controlpoints[1] = new PointD(1000, -800);

        var spline = new BSpline2D(controlpoints);
        var splineControlPoints = spline.getControlPoints();

        assertEquals(2, splineControlPoints.size());
        assertEquals(2, splineControlPoints.get(0).x);
        assertEquals(1, splineControlPoints.get(0).y);
        assertEquals(1000, splineControlPoints.get(1).x);
        assertEquals(-800, splineControlPoints.get(1).y);
    }

    @Test
    void Should_ConstructFromXYArraysCorrectly() {
        var controlpointsX = new int[]{2, 1000};
        var controlpointsY = new int[]{1, -800};

        var spline = new BSpline2D(controlpointsX, controlpointsY);
        var splineControlPoints = spline.getControlPoints();

        assertEquals(2, splineControlPoints.size());
        assertEquals(2, splineControlPoints.get(0).x);
        assertEquals(1, splineControlPoints.get(0).y);
        assertEquals(1000, splineControlPoints.get(1).x);
        assertEquals(-800, splineControlPoints.get(1).y);
    }

    @Test
    void Should_CalculateCorrectPoly() {
        var spline = new BSpline2D(new int[]{}, new int[]{});

        var vec = new VectorND(new double[]{1, 2, 3});
        var polyValue = spline.Polynomial(vec, 4);

        assertEquals(27.0, polyValue);
    }

    @Test
    void Should_getSliceXCorrectly() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(11, 12));
        controlpoints.add(new PointD(21, 22));
        controlpoints.add(new PointD(31, 32));
        controlpoints.add(new PointD(41, 42));
        controlpoints.add(new PointD(51, 52));
        var spline = new BSpline2D(controlpoints);

        var slice = spline.getSliceX(1, 4);

        assertEquals(3, slice.getN());
        assertEquals(21, slice.get(0));
        assertEquals(31, slice.get(1));
        assertEquals(41, slice.get(2));

        slice = spline.getSliceX(1, 2);

        assertEquals(1, slice.getN());
        assertEquals(21, slice.get(0));
    }

    @Test
    void ShouldNot_getSliceXBecauseScliceBeginIsNotGreaterOrEqualThanZero() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(11, 12));
        controlpoints.add(new PointD(21, 22));
        controlpoints.add(new PointD(31, 32));
        controlpoints.add(new PointD(41, 42));
        controlpoints.add(new PointD(51, 52));
        var spline = new BSpline2D(controlpoints);

        assertThrows(AssertionError.class, () -> spline.getSliceX(-1, 1));
    }

    @Test
    void Should_getSliceYCorrectly() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(11, 12));
        controlpoints.add(new PointD(21, 22));
        controlpoints.add(new PointD(31, 32));
        controlpoints.add(new PointD(41, 42));
        controlpoints.add(new PointD(51, 52));
        var spline = new BSpline2D(controlpoints);

        var slice = spline.getSliceY(1, 4);

        assertEquals(3, slice.getN());
        assertEquals(22, slice.get(0));
        assertEquals(32, slice.get(1));
        assertEquals(42, slice.get(2));

        slice = spline.getSliceY(1, 2);

        assertEquals(1, slice.getN());
        assertEquals(22, slice.get(0));
    }

    @Test
    void ShouldNot_getSliceYBecauseScliceBeginIsNotGreaterOrEqualThanZero() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(11, 12));
        controlpoints.add(new PointD(21, 22));
        controlpoints.add(new PointD(31, 32));
        controlpoints.add(new PointD(41, 42));
        controlpoints.add(new PointD(51, 52));
        var spline = new BSpline2D(controlpoints);

        assertThrows(AssertionError.class, () -> spline.getSliceX(-1, 1));
    }


    @Test
    void Should_BeEmpty() {
        var spline = new BSpline2D(new int[]{}, new int[]{});
        assertTrue(spline.isEmpty());
    }

    @Test
    void Should_CalculateCubicPointsCorrectly() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(0, 1));
        controlpoints.add(new PointD(1, 0));
        controlpoints.add(new PointD(0, 1));
        controlpoints.add(new PointD(1, 0));
        var spline = new BSpline2D(controlpoints);

        var cubics = spline.getCubicPoints(0.1);

        assertEquals(12, cubics.size());
        SoftAssertions.isClose(0.666666, cubics.get(0).x, 1e-5);
        SoftAssertions.isClose(0.333333, cubics.get(0).y, 1e-5);
    }

    @Test
    void Should_CalculateSplineLength() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(0, 1));
        controlpoints.add(new PointD(0, 2));
        controlpoints.add(new PointD(0, 3));
        controlpoints.add(new PointD(0, 4));
        controlpoints.add(new PointD(0, 5));
        var spline = new BSpline2D(controlpoints);
        var len = spline.getLength(0.01);
        SoftAssertions.isClose(2, len, 1e-12);
    }

    @Test
    void getValue() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(0, 1));
        controlpoints.add(new PointD(0, 2));
        controlpoints.add(new PointD(0, 3));
        controlpoints.add(new PointD(0, 4));
        controlpoints.add(new PointD(0, 5));
        var spline = new BSpline2D(controlpoints);

        var value = spline.getValue(0.2, 0.1);
        SoftAssertions.isClose(0., value.x, 1e-12);
        SoftAssertions.isClose(3., value.y, 1e-12);
    }
}