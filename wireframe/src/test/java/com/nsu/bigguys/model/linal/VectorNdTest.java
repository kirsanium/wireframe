package com.nsu.bigguys.model.linal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VectorNdTest {

    @Test
    public void Should_ConstructVector() {
        var expectedArray = new double[]{1,2};
        var vector = new VectorND(expectedArray);

        assertArrayEquals(expectedArray, vector.getData());
        assertEquals(expectedArray.length, vector.getN());
    }

    @Test
    public void Should_ConstructVector_WithSize() {
        var expectedArray = new double[]{1,2};
        var n = 4;

        var vector = new VectorND(expectedArray,n);

        assertArrayEquals(expectedArray, vector.getData());
        assertEquals(n, vector.getN());
    }

    @Test
    public void Should_GetAtIndex() {
        var expectedArray = new double[]{1,2};
        var vector = new VectorND(expectedArray);

        var result0 = vector.get(0);
        var result1 = vector.get(1);

        assertEquals(1, result0);
        assertEquals(2, result1);
    }

    @Test
    public void ShouldNot_GetAtIndex_BecauseOutOfRange() {
        var expectedArray = new double[]{1,2};
        var vector = new VectorND(expectedArray);

        assertThrows(AssertionError.class, () -> vector.get(-1));
        assertThrows(AssertionError.class, () -> vector.get(10));
    }
}
