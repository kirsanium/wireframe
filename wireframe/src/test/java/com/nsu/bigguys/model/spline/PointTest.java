package com.nsu.bigguys.model.spline;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void Should_ConstructCorrectFromXAndY() {
        var point = new Point(1, 2);
        assertEquals(1, point.x);
        assertEquals(2, point.y);

        point = new Point(1000, -5000);
        assertEquals(1000, point.x);
        assertEquals(-5000, point.y);
    }

    @Test
    void Should_BeClonedCorrectly() {
        var point = new Point(1, 2);
        var pointCloned = new Point(point);
        assertEquals(1, pointCloned.x);
        assertEquals(2, pointCloned.y);

        point = new Point(1000, -5000);
        pointCloned = new Point(point);
        assertEquals(1000, pointCloned.x);
        assertEquals(-5000, pointCloned.y);
    }

    @Test
    void Should_BeConvertedFromDoubleCorrectly() {
        var pointsD = new ArrayList<PointD>();
        pointsD.add(new PointD(1., 2.));
        pointsD.add(new PointD(1000., -5000.));
        var pointsConverted = Point.fromD(pointsD);

        assertEquals((int) pointsD.get(0).x, pointsConverted.get(0).x);
        assertEquals((int) pointsD.get(1).x, pointsConverted.get(1).x);

        assertEquals(pointsD.size(), pointsConverted.size());
        assertEquals(2, pointsConverted.size());
    }

    @Test
    void Should_CalculateRelativeCoordinatePointsFromScreenPoints() {
        var points = new ArrayList<Point>();
        points.add(new Point(1, 2));
        points.add(new Point(1000, -5000));

        var coordPoints = Point.fromScreenPoints(points);

        assertEquals(2, coordPoints.size());
        assertEquals(-349, coordPoints.get(0).x);
        assertEquals(-298, coordPoints.get(0).y);
        assertEquals(650, coordPoints.get(1).x);
        assertEquals(-5300, coordPoints.get(1).y);
    }
}