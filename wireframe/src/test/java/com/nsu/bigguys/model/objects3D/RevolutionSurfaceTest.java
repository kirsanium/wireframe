package com.nsu.bigguys.model.objects3D;

import com.nsu.bigguys.model.spline.BSpline2D;
import com.nsu.bigguys.model.spline.PointD;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class RevolutionSurfaceTest {

    @Test
    public void Should_ConstructRevolutionSurface() {
        var pointsArray = new PointD[]{new PointD(1,1),new PointD(2,2),new PointD(3,3)};
        var revolutionSurface
                = new RevolutionSurface(
                        new BSpline2D(new PointD[]{new PointD(1,1),new PointD(2,2),new PointD(3,3)}),
                        1, 1, 1, 1, 1, 1, 1);

        var resultControlPoints = revolutionSurface.getGeneratrixCurve().getControlPoints();

        assertNotNull(revolutionSurface.getColor());

        assertThat(resultControlPoints.toArray(new PointD[resultControlPoints.toArray().length]))
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(pointsArray);

        assertArrayEquals(new double[]{1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,}, revolutionSurface.getObjectRotationMatrix().getData());

        assertEquals(1, revolutionSurface.getRotationPrecision());
        assertEquals(1, revolutionSurface.getN());
        assertEquals(1, revolutionSurface.getK());
        assertEquals(1, revolutionSurface.getA());
        assertEquals(1, revolutionSurface.getB());
        assertEquals(1, revolutionSurface.getC());
        assertEquals(1, revolutionSurface.getD());
    }

}
