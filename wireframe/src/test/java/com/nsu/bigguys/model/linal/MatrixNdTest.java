package com.nsu.bigguys.model.linal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MatrixNdTest {

    private final double[] Identity3x3 = new double[]{1,0,0,0,1,0,0,0,1};

    @Test
    public void Should_ConstructMatrix() {
        var expectedArray = Identity3x3;

        var matrix = new MatrixND(expectedArray, 3, 3);

        assertArrayEquals(expectedArray, matrix.getData());
        assertEquals(3, matrix.getN());
        assertEquals(3, matrix.getM());
    }

    @Test
    public void ShouldNot_ConstructMatrix_BecauseOfWrongNM() {
        var expectedArray = Identity3x3;

        assertThrows(AssertionError.class, () -> new MatrixND(expectedArray, 3, 2));
        assertThrows(AssertionError.class, () -> new MatrixND(expectedArray, -1, 2));
        assertThrows(AssertionError.class, () -> new MatrixND(expectedArray, 3, -1));
        assertThrows(AssertionError.class, () -> new MatrixND(expectedArray, -3, -3));
    }

    @Test
    public void Should_MultiplyByVectorNd() {
        var matrix = new MatrixND(Identity3x3, 3, 3);
        var vector = new VectorND(new double[]{1,0,0});

        var result = matrix.matMul(vector);

        assertArrayEquals(new double[]{1,0,0}, result.getData());
    }

    @Test
    public void ShouldNot_MultiplyByVectorNd_BecauseOfN() {
        var matrix = new MatrixND(Identity3x3, 3, 3);
        var vector = new VectorND(new double[]{1,0,0,0});

        assertThrows(AssertionError.class, () -> matrix.matMul(vector));
    }

    @Test
    public void Should_DivideByDouble() {
        var matrix = new MatrixND(new double[]{2,0,0,2}, 2, 2);

        var result = matrix.div(2);

        assertArrayEquals(new double[]{1,0,0,1}, result.getData());
    }

    @Test
    public void Should_GetByIndexes() {
        var matrix = new MatrixND(Identity3x3,3,3);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                assertEquals(i == j ? 1 : 0, matrix.get(i,j));
            }
        }
    }

    @Test
    public void ShouldNot_GetByIndexes_BecauseOutOfBounds() {
        var matrix = new MatrixND(Identity3x3,3,3);

        assertThrows(AssertionError.class, () -> matrix.get(-1, 2));
        assertThrows(AssertionError.class, () -> matrix.get(2, -1));
        assertThrows(AssertionError.class, () -> matrix.get(5, 2));
        assertThrows(AssertionError.class, () -> matrix.get(2, 5));
    }
}
