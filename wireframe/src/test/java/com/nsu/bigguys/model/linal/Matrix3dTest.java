package com.nsu.bigguys.model.linal;

import org.junit.jupiter.api.Test;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Matrix3dTest {

    private double[] Identity4x4 = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
    private double[] Identity3x3 = {1,0,0,0,1,0,0,0,1};

    @Test
    public void Should_ConstructMatrixFrom3x3() {
        double[] data = Identity3x3;

        double[] expectedData = Identity4x4;

        Matrix3D matrix = new Matrix3D(data);

        assertArrayEquals(expectedData, matrix.getData(), 0.00001);
    }

    @Test
    public void Should_ConstructMatrixFrom4x4() {
        double[] expectedData = Identity4x4;

        Matrix3D matrix = new Matrix3D(expectedData);

        assertArrayEquals(expectedData, matrix.getData(), 0.00001);
    }

    @Test
    public void ShouldNot_ConstructMatrix_BecauseInvalidSize() {
        double[] expectedData = {1,0,0,0,1,0,0,0,0,1};

        assertThrows(InvalidParameterException.class, () -> new Matrix3D(expectedData));
    }

    @Test
    public void Should_MultiplyMatrices() {
        var matrix1 = new Matrix3D(Identity4x4);
        var matrix2 = new Matrix3D(Identity4x4);

        var matrix3 = matrix1.matMul(matrix2);

        assertArrayEquals(Identity4x4, matrix3.getData(), 0.00001);
    }

    @Test
    public void Should_GetRotationMatrix() {
        var rotationMatrix = Matrix3D.getRotationMatrix(Math.PI,Math.PI,Math.PI);

        assertArrayEquals(Identity4x4, rotationMatrix.getData(), 0.00001);
    }

    @Test
    public void Should_MultiplyByVector() {
        var matrix = new Matrix3D(Identity4x4);
        var vector = new Vector3D(new double[]{1,1,1,1});

        var result = matrix.matMul(vector);
        assertArrayEquals(new double[]{1,1,1,1}, result.getData());
    }
}
