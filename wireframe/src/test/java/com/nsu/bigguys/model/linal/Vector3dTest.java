package com.nsu.bigguys.model.linal;

import org.junit.jupiter.api.Test;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

public class Vector3dTest {

    private double[] VectorOfOnes3 = {1,1,1};
    private double[] VectorOfOnes4 = {1,1,1,1};

    @Test
    public void Should_ConstructVectorFrom3() {
        var result = new Vector3D(VectorOfOnes3);

        assertArrayEquals(VectorOfOnes4, result.getData());
    }

    @Test
    public void Should_ConstructVectorFrom4() {
        var result = new Vector3D(VectorOfOnes4);

        assertArrayEquals(VectorOfOnes4, result.getData());
    }

    @Test
    public void ShouldNot_ConstructVector_BecauseInvalidSize() {
        double[] expectedData = {1,0,0,0,1,0,0,0,0,1};

        assertThrows(InvalidParameterException.class, () -> new Vector3D(expectedData));
    }

    @Test
    public void Should_ConstructVectorFromDoubles() {
        var result = new Vector3D(1,1,1);

        assertArrayEquals(VectorOfOnes4, result.getData());
    }

    @Test
    public void Should_MultiplyVectorByDouble() {
        var vector = new Vector3D(VectorOfOnes4);

        var result = vector.mul(2);

        assertArrayEquals(new double[]{2,2,2,2}, result.getData());
    }

    @Test
    public void Should_CrossProduct() {
        var vector1 = new Vector3D(VectorOfOnes4);
        var vector2 = new Vector3D(VectorOfOnes4);

        var result = vector1.crossProduct(vector2);

        assertArrayEquals(new double[]{0,0,0,1}, result.getData());
    }

    @Test
    public void Should_SubtractVector() {
        var vector1 = new Vector3D(VectorOfOnes4);
        var vector2 = new Vector3D(VectorOfOnes4);

        var result = vector1.subtract(vector2);

        assertArrayEquals(new double[]{0,0,0,0}, result.getData());
    }

    @Test
    public void Should_AddVector() {
        var vector1 = new Vector3D(VectorOfOnes4);
        var vector2 = new Vector3D(VectorOfOnes4);

        var result = vector1.add(vector2);

        assertArrayEquals(new double[]{2,2,2,1}, result.getData());
    }

    @Test
    public void Should_FindNorm() {
        var vector = new Vector3D(VectorOfOnes3);

        var result = vector.norm();

        assertEquals(Math.sqrt(3), result);
    }

    @Test
    public void Should_Normalize() {
        var vector = new Vector3D(VectorOfOnes3);

        vector.normalize();

        assertArrayEquals(new double[]{1/Math.sqrt(3d), 1/Math.sqrt(3d), 1/Math.sqrt(3d), 1}, vector.getData());
    }

    @Test
    public void Should_ReturnNormalizedVector() {
        var vector = new Vector3D(VectorOfOnes3);

        var result = vector.normalized();

        var expectedVector = new Vector3D(new double[]{1/Math.sqrt(3d), 1/Math.sqrt(3d), 1/Math.sqrt(3d), 1});

        assertArrayEquals(expectedVector.getData(), result.getData());
    }

    @Test
    public void Should_Clone() {
        var vector = new Vector3D(VectorOfOnes3);

        var result = new Vector3D(vector);

        assertArrayEquals(vector.getData(), result.getData());
    }

    @Test
    public void Should_GetEverything() {
        var vector = new Vector3D(VectorOfOnes3);

        assertEquals(1, vector.getX());
        assertEquals(1, vector.getY());
        assertEquals(1, vector.getZ());
    }

    @Test
    public void Should_SetEverything() {
        var vector = new Vector3D(VectorOfOnes3);

        vector.setX(0);
        vector.setY(0);
        vector.setZ(0);

        assertArrayEquals(new double[]{0,0,0,1}, vector.getData());
    }
}
