package com.nsu.bigguys.model.spline;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointDTest {
    @Test
    void Should_ConstructCorrectFromXAndY() {
        var pointD = new PointD(1.0, 2.0);
        assertEquals(1.0, pointD.x);
        assertEquals(2.0, pointD.y);

        pointD = new PointD(1000., -5000.);
        assertEquals(1000., pointD.x);
        assertEquals(-5000., pointD.y);
    }
}