package com.nsu.bigguys.model.objects3D;

import com.nsu.bigguys.model.linal.Vector3D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class AABBTest {

    @Test
    public void Should_ConstructAabbNoParams() {
        var aabb = new AABB();
        var expectedArray = new double[]{0,0,0,1};

        assertArrayEquals(expectedArray, aabb.minValues.getData());
        assertArrayEquals(expectedArray, aabb.maxValues.getData());
    }

    @Test
    public void Should_ConstructAabbWithParams() {
        var expectedArray = new double[]{0,0,0,1};
        var vector = new Vector3D(expectedArray);
        var aabb = new AABB(vector);

        assertArrayEquals(expectedArray, aabb.minValues.getData());
        assertArrayEquals(expectedArray, aabb.maxValues.getData());
    }

    @Test
    public void Should_CheckPoints() {
        var biggerVector = new Vector3D(new double[]{1,2,3});
        var smallerVector = new Vector3D(new double[]{-1,-2,-3});
        var aabb = new AABB();

        aabb.checkPoints(biggerVector);
        aabb.checkPoints(smallerVector);

        assertArrayEquals(biggerVector.getData(), aabb.maxValues.getData());
        assertArrayEquals(smallerVector.getData(),aabb.minValues.getData());
    }

}
