package com.nsu.bigguys;

import static org.junit.jupiter.api.Assertions.*;

public class SoftAssertions {
    static public void isClose(double expected, double actual, double error) {
        assertTrue(expected - error <= actual + error);
        assertTrue(expected + error >= actual - error);
    }
}
