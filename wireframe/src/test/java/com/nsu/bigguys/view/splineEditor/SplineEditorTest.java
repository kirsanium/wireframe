package com.nsu.bigguys.view.splineEditor;

import com.nsu.bigguys.SoftAssertions;
import com.nsu.bigguys.model.spline.Point;
import com.nsu.bigguys.model.spline.PointD;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SplineEditorTest {

    @Test
    void Should_CalculateCorrectEuclidianMetric() {
        var begin = new Point(0, 0);
        var end = new Point(1, 1);

        SoftAssertions.isClose(1.414, SplineEditor.euclidianMetric(begin, end), 1e-3);
    }

    @Test
    void Should_FindNearestPointCorrectly() {
        var controlpoints = new ArrayList<PointD>();
        controlpoints.add(new PointD(0, 1));
        controlpoints.add(new PointD(0, 2));

        var editor = new SplineEditor(controlpoints);

        int minid = editor.findNearestPoint(new Point(-3, -5));
        assertEquals(0, minid);
    }
}